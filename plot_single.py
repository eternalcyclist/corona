#!/usr/bin/python
# vim: fileencoding=utf8
import einwohner
from pylab import *
from IPython import embed
import traceback
from plot_nowcasting import str2xpos
import datetime
from utils import *

quellentext=u"Daten:Robert Koch-Institut (RKI), dl-de/by-2-0"

LINEWIDTH = 3

resultspath="results/"

def umlautconv(word):
    word=word.replace("ü","ue")	
    word=word.replace("ö","oe")	
    word=word.replace("ä","ae")	
    word=word.replace("ß","ss")	
    return word

def beautify(lkr):
    pre,post=lkr.split(" ",1)
    post=umlautconv(post)
    if pre=="sk":
        pre = "Stadtkreis"
    elif pre=="lk":
        pre = "Landkreis"
    post = post.capitalize()
    return "%s %s"%(pre,post)

def plot_absolut(alle_faelle,alle_todesfaelle,names,events=[],
                 bl=r"Baden-Württemberg",
                 startdate=None,
                 enddate=None,
                 do_plot_tote=False,
                 ymax=None,
                ):
    try:
        fignames=["summe","inzidenz"]
        faelle = alle_faelle[-1]
        todesfaelle = alle_todesfaelle[-1]
        blname=bl
        bl = bl.encode("utf8")
        bl = bl.lower()
        bl_fall = alle_faelle[1][bl]
        bl_todesfall = alle_todesfaelle[1][bl]
        bl_ew=einwohner_bl[bl]
        bl = bl.decode("utf8")
        years = bl_ew.keys()
        bl_ew = bl_ew[max(years)]*1000
    except:
        traceback.print_exc()
        embed()
    for lkr in faelle.keys():
        try:
            if len(names)!=0:
                do_plot=0
                for name in names:
                    if name.lower() in lkr.lower():
                        do_plot=1
            else:
                do_plot=1
            if not do_plot:
                continue
            try:
                ew = einwohner_lkr[lkr]["ew"]
            except KeyError:
                print ("%s not found"%lkr)
                continue
            for  fname in fignames:
                figure(fname,figsize=(12,6))
                clf()
                grid(1)
            positive = cumsum(faelle[lkr]["total"][1])*100000.0/ew
            ptimes = faelle[lkr]["total"][0]
            bl_positive = cumsum(bl_fall["total"][1])*100000.0/bl_ew
            bl_ptimes = bl_fall["total"][0]
            try:
                tote = cumsum(todesfaelle[lkr]["total"][1])*100000.0/ew
                ttimes = todesfaelle[lkr]["total"][0]
                bl_tote = cumsum(bl_todesfall["total"][1])*100000.0/bl_ew
                bl_ttimes = bl_todesfall["total"][0]
            except:
                tote=[]
                ttimes=[]

            for fname in fignames:     
                figure(fname)
            
            fig=figure("summe")
            plot(ptimes,positive,"b",lw=LINEWIDTH,label="Positiv Getestete")

            quellenangabe(fig,quellentext)
            ylabel("Summe Positiv Getesteter / 100000 Ew")
            legend(loc=2,framealpha=0.7)
            twinx()
            plot(ttimes,tote,"r",lw=2,label="Verstorbene")
            if startdate!=None:
                try:
                    xmin=datetime.date(startdate)
                except:
                    xmin=startdate
            else:
                xmin=xlim()[0]
            if enddate!=None:
                try:
                    xmax=datetime.date(enddate)
                except:
                    xmax=enddate
            else:
                xmax=xlim()[1]
            xlim(xmin,xmax)
            legend(loc=1,framealpha=0.7)
            ylabel("Summe Verstorbener / 100000 Ew")
            title("%s Summe"%beautify(lkr))
            savefig("%s/Corona_Summe_%s.jpg"%(resultspath,lkr))
            close()

            fig=figure("inzidenz")
            plot(ptimes[6:],positive[6:]-positive[:-6],"b",lw=LINEWIDTH,label="Positiv Getestete in %s"%names[0])
            plot(bl_ptimes[6:],bl_positive[6:]-bl_positive[:-6],"b",lw=2*LINEWIDTH,alpha=0.2,label=u"Positiv Getestete %s"%blname)
            if ymax==None:
                ymin,ymax=ylim()
            else:
                ymin = ylim()[0]
            quellenangabe(fig,quellentext)
            for event in events:
                try:
                    #bar(event.date,height=ylim()[-1],color="k")
                    edate = str2xpos(event[0])
                    plot([edate,edate],[ymin,ymax],color="k",lw=2,ls="--",alpha=0.2)
                    text(edate+datetime.timedelta(1),(ymin+ymax)/2,event[1],rotation=90.0,alpha=0.6
                         ,horizontalalignment="left"
                        ,verticalalignment="center"
                        ,fontsize=12)
                except:
                    traceback.print_exc()
                    embed()

            ylabel("7-Tage Inzidenz: Positiv Getestete / 100000 Ew")
            legend(loc=2,framealpha=0.7)
            if ymax==None:
                ylim(0,ylim()[-1])
            else:
                ylim(0,ymax)
            if do_plot_tote:
                twinx()
                if len(tote)>6:
                    plot(ttimes[6:],tote[6:]-tote[:-6],"r",lw=LINEWIDTH,label="Verstorbene")
                if len(bl_tote)>6:
                    plot(bl_ttimes[6:],bl_tote[6:]-bl_tote[:-6],"r+-",lw=1.3*LINEWIDTH,alpha=0.2,label=u"Verstorbene %s"%blname)
                legend(loc=1,framealpha=0.7)
                ylabel('7-Tage-Anteil Verstorbene / 100000 Ew')
            if startdate!=None:
                try:
                    xmin=datetime.date(startdate)
                except:
                    xmin=startdate
            else:
                xmin=xlim()[0]
            if enddate!=None:
                try:
                    xmax=datetime.date(enddate)
                except:
                    xmax=enddate
            else:
                xmax=xlim()[1]
            xlim(xmin,xmax)
            ylim(0,ylim()[-1])
            title("%s 7-Tage Inzidenz"%beautify(lkr))
            savefig("%s/Corona_Inzidenz_%s.jpg"%(resultspath,lkr))
            close()
        except:
            traceback.print_exc()
            embed()

def plot_absolut_weekly(data,names):
    for lkr,lkrobj in data.lkr.items():
        do_plot=0
        if len(names)!=0:
            do_plot=0
            for name in names:
                if name.lower() in lkr.lower():
                    do_plot=1
        else:
            do_plot=1
        if not do_plot:
            continue
        print(lkr) 
        tendtime = lkrobj.todesfallendtime-1
        fendtime = lkrobj.fallendtime-1
        ttime = data.time[:tendtime]
        ftime = data.time[:fendtime]
        infizierte = cumsum(lkrobj.fall[:fendtime])
        tote = cumsum(lkrobj.todesfall[:fendtime])
        figure(figsize=(10,6))
        grid()
        xlabel("Tage seit 1. Maerz")
        title("Woechentliche Zunahme "+ umlautconv(lkr)+" "+lkrobj.bl)
        plot(ftime[7:],infizierte[7:]-infizierte[:-7],"b",label="Positiv Getestete")
        ylabel("Positiv Getestete")
        legend(loc=2,framealpha=0.7)
        twinx()
        plot(ttime[7:],tote[7:]-tote[:-7],"r",label="Gestorbene")
        xlim(0,data.endtime)
        legend(loc=1,framealpha=0.7)
        ylabel("Gestorbene")
        savefig("%s/Corona_absolut_woechentliche_%s.jpg"%(resultspath,lkr+"_"+lkrobj.bl))
        close()

def plot_faktor_weekly(data,names):
    for lkr,lkrobj in data.lkr.items():
        do_plot=0
        if len(names)!=0:
            do_plot=0
            for name in names:
                if name.lower() in lkr.lower():
                    do_plot=1
        else:
            do_plot=1
        if not do_plot:
            continue
        print(lkr)
        tendtime = lkrobj.todesfallendtime-1
        fendtime = lkrobj.fallendtime-1
        ttime = data.time[:tendtime]
        ftime = data.time[:fendtime]
        infizierte = cumsum(lkrobj.fall[:fendtime])
        tote = cumsum(lkrobj.todesfall[:fendtime])
        figure(figsize=(10,6))
        grid()
        xlabel("Tage seit 1. Maerz")
        title("Woechentlicher Steigerungsfaktor "+ umlautconv(lkr)+" "+lkrobj.bl)
        plot(ftime[7:],infizierte[7:]/infizierte[:-7],"b",label="Postiv Getestete")
        ylim(0,20)
        ylabel("Faktor Positiv Getestete")
        legend(loc=2,framealpha=0.7)
        twinx()
        plot(ttime[7:],tote[7:]/tote[:-7],"r",label="Gestorbene")
        xlim(0,data.endtime)
        ylim(0,20)
        legend(loc=1,framealpha=0.7)
        ylabel("Faktor Gestorbene")
        savefig("%s/Corona_steigerung_woechentliche_%s.jpg"%(resultspath,lkr+"_"+lkrobj.bl))
        close()

if __name__=="__main__":
    import sys, cPickle
    names = sys.argv[1:]
    events=[
        #("15.2.2020",u"Karnevalssitzung in Gangelt"),
        #("28.2.2020",u"Schulschließungen im LK Heinsberg"),
        ("23.3.2020",u"Bundesweite Kontaktbeschränkungen"),
        ("29.4.2020",u"Bundsweite Maskenpflicht"),
        ("02.Nov.2020",u"Lockdown Light"),
        #("16.Nov.2020",u"Erwarteter Wirkungseintritt Lockdown Light"),
        ("16.dec.2020",u"Verschärfter Lockdown"),
        #("26.dec.2020",u"Beginn der Impfungen"),
        #("31.dec.2020",u"Impfbeginn im Pflegeheim Uhldingen-Mühlhofen"),
        #("13.feb.2021",u"Fasnetsumzug in Überlingen"),
        #("30.dec.2020",u"Erwarteter Wirkungseintritt versch. Lockdown"),
        #("16.jan.2021",u"Wandergruppe Mühlheim"),
    ]

    faelle = cPickle.load(open("datasources/faelle_lkr.rki"))
    todesfaelle = cPickle.load(open("datasources/todesfaelle_lkr.rki"))
    bl_faelle = cPickle.load(open("datasources/faelle_bl.rki"))
    bl_todesfaelle = cPickle.load(open("datasources/todesfaelle_bl.rki"))
    d_faelle = cPickle.load(open("datasources/faelle_gesamt.rki"))
    d_todesfaelle = cPickle.load(open("datasources/todesfaelle_gesamt.rki"))
    einwohner_lkr=einwohner.loader(tolower=True)
    einwohner_bl=einwohner.load_laender(tolower=True)

    plot_absolut((d_faelle,bl_faelle,faelle),(d_todesfaelle,bl_todesfaelle,todesfaelle),names,events=events,
                 #bl="Nordrhein-Westfalen",
                 bl=u"Baden-Württemberg",
                 #bl="Bayern",
                 #bl="Sachsen-Anhalt",
                 #bl="Rheinland-Pfalz",
                 startdate=datetime.date(2020,3,1),
                 #startdate=datetime.date(2020,12,25),
                 enddate=datetime.date(2021,3,7),
                 do_plot_tote=True,
                 #ymax=155,
                )

    #plot_absolut_weekly(faelle,todesfaelle,names)
    #plot_faktor_weekly(faelle,todesfaell,names)
