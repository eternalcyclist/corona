#!/usr/bin/python

from pylab import *
import einwohner

def plot_ew_altersverlauf((x,y),name):
    print((len(x),len(y)))
    bar(x,y,width=5)
    title("Einwohner Altersverteilung %s"%name)
    savefig("Einwohneraltersverteilung_%s.png"%name)
    show()

if __name__=="__main__":
    import sys
    selected = sys.argv[1:]
    data = einwohner.loader(binwidth=5)
    for key,item in data.items():
        for sel in selected:
            if sel.lower() in key.lower():
                plot_ew_altersverlauf(item["alter"],key)

