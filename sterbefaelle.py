#!/usr/bin/python 
# vim: fileencoding=utf8
"""
Auswertung der Sonderauswertung Sterbefälle
"""

import pandas 
import IPython
from IPython import embed
import traceback
import scipy.optimize as so
from pylab import *
from utils import *
from isoweek import Week
import calendar
import bevoelkerung

quellentext=u"Daten: Statistisches Bundesamt Sonderauswertung Sterbefälle Stand Februar 2021"
filename = "datasources/sonderauswertung-sterbefaelle.xlsx"

def load_days(startyear=2016,endyear=2021):
    data={"dates":[],"Gestorbene":[]}
    df = pandas.read_excel(filename,"D_2016_2021_Tage",header=8,index_col=0)
    columns=df.columns
    for rowname,rowvals in zip(df.index[-1::-1],df.values[-1::-1]):
        if rowname<startyear or rowname>endyear: 
            continue
        for dm,val in zip(columns,rowvals):
            if "Insgesamt" in dm:
                continue
            if val=="X":
                continue
            try:
                day,month=map(int,dm[:-1].split("."))
            except:
                traceback.print_exc()
                embed()
            try:
                date = pandas.datetime(rowname,month,day)
            except ValueError:
                traceback.print_exc()
                print("continuing ...")
                continue
            except:
                traceback.print_exc()
                embed()
            data["dates"].append(date)
            data["Gestorbene"].append(val)
    return data

def load_insgesamt(startyear=2016,endyear=2021):
    df = pandas.read_excel(filename,"D_2016_2020_Tage",header=8,index_col=0)

def load_altersgruppen_monat():
    df = pandas.read_excel(filename,"D_2016-2021_Monate_AG_Ins",skip_rows=range(9),index_col=None)
    ags ={}
    lasty ={}   # temporäre dict
    months = df.values[7][3:]
    dates=[]
    for i in range(8,len(df.values)):
        vals = df.values[i]
        y=int(vals[1])
        ag=vals[2]
        if not ags.has_key(ag):
            ags[ag]=[]
            lasty[ag]=y
        v = list(vals[3:-1])
        if ag=="Insgesamt":
            newdates=[]
            for monat in range(1,len(v)+1):
                try:
                    newdates.append(datetime.date(y,monat,calendar.monthrange(y,monat)[-1]))
                    # WARNING: Ist es richtig, den Month Value auf das Ende de Monat zu setzen?
                except:
                    traceback.print_exc()
                    embed()
        if lasty[ag]<=y:
            ags[ag].extend(v)
            if ag=="Insgesamt":
                dates.extend(newdates)
            print("append")
        else:
            v.extend(ags[ag][:])
            ags[ag]=v
            if ag=="Insgesamt":
                newdates.extend(dates) 
                dates=newdates
        lasty[ag]=y
    return dates,ags    

def load_altersgruppen_jahr(bins=None):
    dates,ags=load_altersgruppen_monat()
    oyears=range(dates[0].year,dates[-1].year)
    odates=[datetime.datetime(y,12,31) for y in oyears]
    oags={}
    for ag,vals in ags.items():
        ovals=zeros(len(odates))
        for d,v in zip(dates,vals):
            try:
                ix = oyears.index(d.year)
            except ValueError:
                print("Ignoring %s"%d.year)
                continue
            ovals[ix]+=v
        oags[ag]=ovals
    if bins==None:
        return odates,oags
    else:
        return odates,agregroup(bins,oags)


def load_altersgruppen_woche(bins=None):
    df = pandas.read_excel(filename,"D_2016_2021_KW_AG_Ins",skip_rows=range(9),index_col=None)
    ags ={}
    lasty ={}   # temporäre dict
    kws = df.values[7][3:]
    dates=[]
    for i in range(8,len(df.values)):
        vals = df.values[i]
        y=int(vals[1])
        ag=vals[2]
        if not ags.has_key(ag):
            ags[ag]=[]
            lasty[ag]=y
        v = list(vals[3:])
        if u"X " == v[-1]:
            v=v[:-1]
        if ag=="Insgesamt":
            try:
                mask = where(isfinite(v))
            except:
                traceback.print_exc()
                embed()
        v = take(v,mask)[0]
        v = v.tolist()
        if ag=="Insgesamt":
            newdates=[]
            for kw in range(1,len(v)+1):
                newdates.append(Week(y,kw).sunday())
                # WARNING: Ist es richtig, den Week Value auf das Ende der Week zu setzen:w
        if lasty[ag]<=y:
            ags[ag].extend(v)
            if ag=="Insgesamt":
                dates.extend(newdates)
            print("append")
        else:
            v.extend(ags[ag][:])
            ags[ag]=v
            if ag=="Insgesamt":
                newdates.extend(dates) 
                dates=newdates
        lasty[ag]=y
    if bins==None:
        return dates,ags    
    else:
        return dates,agregroup(bins,ags)

def load_gesamt_woche():
    dates,ags=load_altersgruppen_woche()
    return dates,ags["Insgesamt"]

def plot_altersgruppen_woche():
    dates,ags=load_altersgruppen_woche()
    fig=figure(figsize=(10,6))
    grid(1)
    plot(dates,ags["Insgesamt"],lw=3)
    ylabel(u"Wöchentliche Sterbefälle absolut")
    title(u"Wöchentliche Sterbefälle in Deutschland")
    quellenangabe(fig,quellentext)
    ylim(0,ylim()[1])
    savefig("results/Sonderauswertung_Sterbefaelle_woche_insgesamt.jpg")

    fig=figure(figsize=(10,6))
    grid(1)
    keys = ags.keys()
    keys.sort()
    for i,ag in enumerate(keys):
        if ag=="Insgesamt": continue
        c = cm.jet((i+1)*1./len(keys))
        plot(dates,ags[ag],lw=2,label=ag,color=c)
    legend()
    ylabel(u"Wöchentliche Sterbefälle absolut")
    title(u"Wöchentliche Sterbefälle nach Altersgruppen in Deutschland")
    quellenangabe(fig,quellentext)
    ylim(0,ylim()[1])
    savefig("results/Sonderauswertung_Sterbefaelle_Altersgruppen_woche.jpg")

    fig=figure(figsize=(10,6))
    grid(1)
    keys = ags.keys()
    keys.sort()
    gesamt = ags["Insgesamt"]
    for i,ag in enumerate(keys):
        if ag=="Insgesamt": continue
        c = cm.jet((i+1)*1./len(keys))
        plot(dates,array(ags[ag])*100./gesamt,lw=2,label=ag,color=c)
    legend()
    ylabel(u"Wöchentliche Sterbefälle / %")
    title(u"Verteilung der wöchentlichen Sterbefälle nach Altersgruppen in Deutschland")
    quellenangabe(fig,quellentext)
    ylim(0,ylim()[1])
    savefig("results/Sonderauswertung_Sterbefaelle_Altersgruppen_woche_relativ.jpg")

def plot_altersgruppen_woche_relativ_bev():
    bins=[30,40,50,60,70,80,90,999]
    dates,ags=load_altersgruppen_woche(bins=bins)
    bdates,agb=bevoelkerung.load(dateoffset=False,bins=bins)
    try:
        """ TODO:
        wir mussen die Bevoelkerungszahlen mittels der jeweiligen
        Sterbezahlen finesampeln.
        """
        bstartdate=datetime.datetime(dates[0].year-1,12,31)
        bendyear=dates[-1].year
        benddate=datetime.datetime(dates[-1].year-1,12,31)
        # wir nehmen die Bevölkerungszahl des Vorjahres als Referenz
        bstart = bdates.index(bstartdate)
        bend = bdates.index(benddate)
    except:
        traceback.print_exc()
        embed()
    fig=figure(figsize=(10,6))
    grid(1)
    keys = ags.keys()
    keys.sort()
    try:
        for i,ag in enumerate(keys):
            if ag=="Insgesamt": continue
            c = cm.jet((i+1)*1./len(keys))
            plot(dates,array(ags[ag])*100./array(agb[ag])[bstart:bend],lw=2,label=ag,color=c)
    except:
        traceback.print_exc()
        embed()
    legend()
    ylabel(u"Wöchentliche Sterbefälle / %")
    title(u"Anteil der Sterbefälle an der Bevölkerung nach Altersgruppen in Deutschland")
    quellenangabe(fig,quellentext + u"und dessen Bevölkerunstabellen.\nWarnung: Aufgrund methodischer Änderungen sind die Einwohnerzahlen nur bedingt vergleichbar.")
    ylim(0,ylim()[1])
    savefig("results/Sonderauswertung_Sterbefaelle_Altersgruppen_woche_relativ_bev.jpg")

def plot_altersgruppen_monat():
    dates,ags=load_altersgruppen_monat()
    fig=figure(figsize=(10,6))
    grid(1)
    plot(dates,ags["Insgesamt"],"-o",lw=3)
    ylabel(u"Monatliche Sterbefälle absolut")
    title(u"Monatliche Sterbefälle in Deutschland")
    quellenangabe(fig,quellentext)
    ylim(0,ylim()[1])
    savefig("results/Sonderauswertung_Sterbefaelle_monat_insgesamt.jpg")

    fig=figure(figsize=(10,6))
    grid(1)
    keys = ags.keys()
    keys.sort()
    for i,ag in enumerate(keys):
        if ag=="Insgesamt": continue
        c = cm.jet((i+1)*1./len(keys))
        plot(dates,ags[ag],"-o",lw=2,label=ag,color=c)
    legend()
    ylabel(u"Monatliche Sterbefälle absolut")
    title(u"Monatliche Sterbefälle nach Altersgruppen in Deutschland")
    quellenangabe(fig,quellentext)
    ylim(0,ylim()[1])
    savefig("results/Sonderauswertung_Sterbefaelle_Altersgruppen_monat.jpg")


def plot_altersgruppen_jahr():
    dates,ags=load_altersgruppen_jahr()
    x=array([d.year for d in dates])
    fig=figure(figsize=(10,6))
    grid(0)
    gesamt = ags["Insgesamt"]
    bar(x,gesamt) 
    ylabel(u"Sterbefälle")
    title(u"Jährliche Sterbefälle in Deutschland")
    savefig("results/Sonderauswertung_Sterbefaelle_insgesamt_jahr.jpg")

    fig=figure(figsize=(10,6))
    grid(axis="y")
    keys = ags.keys()
    keys.sort()
    keys.pop(keys.index("Insgesamt"))
    for i,ag in enumerate(keys):
        c=cm.jet(i*1.0/len(keys))
        bar(x-0.4+i*1./20,ags[ag],width=1./25,label=ag,alpha=0.9,color=c) 
    legend()
    xlim(2015,xlim()[1])
    ylabel(u"Sterbefälle")
    title(u"Jährliche Sterbefälle in Deutschland nach Altersgruppen (alle Ursachen)")
    quellenangabe(fig,quellentext)
    savefig("results/Sonderauswertung_Sterbefaelle_Altersgruppen_jahr.jpg")

def plot_altersgruppen_jahr_rel_bev(
    bins=[30,40,50,60,70,80,90,999]
    ):
    
    dates,ags=load_altersgruppen_jahr(bins=bins)
    x=array([d.year for d in dates])

    fig=figure(figsize=(10,6))
    grid(axis="y")
    keys = ags.keys()
    keys.sort()
    for i,ag in enumerate(keys):
        c=cm.jet(i*1.0/len(keys))
        bar(x-0.35+i*1./10,ags[ag],width=1./13,label=agname(i,keys),alpha=0.9,color=c) 
    legend(loc=2)
    xlim(2015,xlim()[1])
    ylabel(u"Sterbefälle")
    title(u"Jährliche Sterbefälle in Deutschland nach Altersgruppen (alle Ursachen)")
    quellenangabe(fig,quellentext)
    savefig("results/Sonderauswertung_Sterbefaelle_Altersgruppen_jahr_rebinned.jpg")

    try:
        bdates,agb=bevoelkerung.load(dateoffset=False,bins=bins,startdate=2015)
        print(u"ACHTUNG! Für die Bevölkerungszahl wird der Jahresendstand des Vorjahres genommen.")
        fig=figure(figsize=(10,6))
        grid(axis="y")
        keys = ags.keys()
        keys.sort()
        for i,ag in enumerate(keys):
            c=cm.jet(i*1.0/len(keys))
            endidx=min(len(ags[ag]),len(agb[ag]))
            y=ags[ag][:endidx]*100.0/agb[ag][:endidx]
            bar(x-0.35+i*0.9/len(bins),y,width=0.8/len(bins),label=agname(i,keys),alpha=0.9,color=c) 
        legend(loc=2)
        xlim(2015,xlim()[1])
        ylabel(u"Sterberate in Prozent der Einwohner der Altersgruppe")
        title(u"Jährliche Sterberaten in Deutschland nach Altersgruppen (alle Ursachen)")
        quellenangabe(fig,quellentext + u"\nund dessen Bevölkerunstabellen.\nWarnung: Aufgrund methodischer Änderungen sind die Einwohnerzahlen nur bedingt vergleichbar.")
        savefig("results/Sonderauswertung_Sterberate_Altersgruppen_jahr_rel_bev.jpg")
    except:
        traceback.print_exc()
        embed()


def smooth(vals):

    def curve(x,off,steigung,per,ampl,phi):
        """
        x - Tage
        """
        #return off+steigung*x + ampl*sin(x*per+phi)
        return off+steigung*x + ampl*sin(6.28*x/360+phi)


    vals = 1.0*array(vals)
    x = arange(len(vals),dtype=float) 
    popt,dummy = so.curve_fit(curve,x,vals,(80000,10,10000,2*3.14/360,3.14/2))
    print(popt)
    svals = curve(x,*popt)
    return svals

def plot_alter(data,name,ytext="Gestorbene / Tag",smoothing=True):
    if smoothing:
        figure(name+"s",figsize=(12,7))
        clf()
        grid()
        ylabel(ytext)
    figure(name,figsize=(12,7))
    clf()
    grid()
    keys=data.keys()
    keys.sort()
    ylabel(ytext)
    lw=1
    alpha=1
    color="g"
    dates=data["dates"]
    for key in keys:
        if key=="dates":
            continue
        if len(keys)>5:
            color=cm.jet((keys.index(key)-1)*1.0/(len(keys)-3))
        plot(dates,data[key],label=key,lw=lw,alpha=alpha,color=color)
        if smoothing:
            sd = smooth(data[key])
            plot(data["dates"],sd,ls="--",lw=lw,alpha=alpha,color="k",label=key+" est")
            figure(name+"s")
            plot(data["dates"],data[key]-sd,ls="-",lw=lw,alpha=alpha,color=color,label=key+" diff")
            figure(name)
    legend(loc=2)
    xlim(min(dates),max(dates))
    title(name)
    savefig("%s.png"%name)
    if smoothing:
        figure(name+"s")
        legend(loc=2)
        title(name + " - Difference to estimated")
        savefig("%s_ediff.png"%name)

def plot_alter_relativ(data,name):
    """ plotte den prozentualen Anteil der Altersgruppen an der Gesamtzahl der Gestorbenen
    """
    figure(name,figsize=(12,7))
    clf()
    grid()
    try:
        t = data.pop("dates")
        insgesamt = array(data.pop("Insgesamt"))
        keys=data.keys()
        keys.sort()
    except:
        traceback.print_exc()
        embed()
    for key in keys:
        if "mehr" in key:
            color = "k"
        else:
            color = cm.jet((keys.index(key)-1)*1.0/(len(keys)-3))
        plot(t,array(data[key])*100.0/insgesamt,label=key,color=color)
        legend(loc=2)
    xlim(min(t),max(t))
    ylabel("Anteil Gestorbener / %%")
    title(name)
    savefig("%s.png"%name)

def stabu(startyear=2016,endyear=2021):
    figure("stabu")
    clf()
    grid()
    years=range(startyear,endyear)
    summentote=zeros(365-7)
    ydata=[]
    dates=None
    for year in years:
        data = load_days(year,year)
        print(len(data))
        data = average(data, 7)
        idata=data["Gestorbene"][:365-7]
        ydata.append(idata)
        if year<endyear-1:
            summentote+=idata
            dates=data["dates"]
    avgtote=summentote*1.0/(endyear-startyear-1)
    colors=("b","g","tab:pink","tab:orange")
    for y,d in zip(years,ydata):
        l = min(len(d),365)
        if y==2020:
            alpha=1
            color="r"
        else:
            alpha=0.5
            try:
                color=colors[years.index(y)]
            except IndexError:
                color="k"
        print("plotting %s"%y)
        plot(dates[:l],d[:l]*100.0/avgtote[:l]-100,label=y,color=color,alpha=alpha)
    legend(loc=2)
    ylabel("Abweichung zum Mittelwert %s bis %s"%(startyear,endyear-2)) 
    savefig("stabu.png")

def average(data, interval):
    outdata={}
    for key,vals in data.items():
        if key=="dates":
            outdata["dates"]=vals[interval:]
            continue
        vsum = cumsum(vals)
        outdata[key]=(vsum[interval:]-vsum[:-interval])/interval
    return outdata

def merge_age_groups(data,limits):
    odata={}
    keys = data.keys()
    keys.sort()
    for key in keys:
        if key in ("Insgesamt","dates"):
            odata[key]=data[key]
            continue
        if "mehr" in key:
            okey="%s u. mehr"%limits[-1]
        else:
            keyend=int(key.split()[-1])
            if len(limits)>1:
                raise Exception("mehr als ein limit geht so nicht")
            okey=None
            for lix,limit in enumerate(limits):
                if keyend<limit:
                    if lix==0:
                        okey="0-%s"%limit
                    else:
                        okey="%s-%s"%(limits[lix-1],limit)
            if okey==None:
                okey="%s u. mehr"%limits[-1]
        if not okey in odata:
            odata[okey]=array(data[key])
        else:
            odata[okey]+=array(data[key])  
    return odata

if __name__=="__main__":
    from sys import exit

    #plot_altersgruppen_jahr_rel_bev()
    plot_altersgruppen_jahr_rel_bev(bins=[45,65,85,999])
    plot_altersgruppen_jahr()
    #plot_altersgruppen_jahr()
    #plot_altersgruppen_woche_relativ_bev()
    exit()
    1/0
    plot_altersgruppen_monat()
    plot_altersgruppen_woche()
    exit()
    stabu()
    d=load_days()
    plot_alter_relativ(d.copy(),"Relativer Anteil der Altersgruppen")
    dm = merge_age_groups(d,[80]) 
    plot_alter_relativ(dm.copy(),"Relativer Anteil der kombinierten Altersgruppen")
    dm7 = average(dm,7)
    plot_alter_relativ(dm7.copy(),"Relativer Anteil der kombinierten Altersgruppen mAvg7")
    d7 = average(d,7) 
    plot_alter_relativ(d7.copy(),"Relativer Anteil der Altersgruppen mAvg7")
    insgesamt=d.pop("Insgesamt")
    dates=d["dates"]
    o={"Insgesamt":insgesamt,"dates":dates}
    plot_alter(d,"Sterbefaelle nach Alter")
    plot_alter(o,"Sterbefaelle in Deutschland insgesamt",smoothing=False)
    insgesamt7=d7.pop("Insgesamt")
    dates=d7["dates"]
    o7={"Insgesamt":insgesamt7,"dates":dates}
    plot_alter(d7,"Sterbefaelle 7 Tage mAvg nach Alter",ytext="Gestorbene / Tag")
    plot_alter(o7,"Sterbefaelle 7 Tage mAvg Overview",ytext="Gestorbene / Tag")
    ion()
    show()
    IPython.embed()
