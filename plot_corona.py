#!/usr/bin/python
# vim: fileencoding=utf-8
"""
"""
endtime=100
plot_todesfall=True
#plot_todesfall=False
#do_label_all=True
do_label_all=False

import sys
import traceback
from pylab import *
import einwohner
import rki
import cPickle


colors = ["r","b","g","y","m","c","#ffaa00","#00ffaa","#0055ff","#f03af3","#44ff00"]

colordict={
"hamburg":("r",":"),
"muc":("b","--"),
"freising":("#8888ff","--"),
"heinsberg":("c",":"),
"tirschenreuth":("y","--"),
"stuttgart":("g","-"),
"freiburg":("m","-"),
"sigmaringen":("#ff8888","-"),
"zollernalb":("#ffaa00","-"),
"rheinneckar":("#77ff77","-"),
"karlsruhe_land":("#00ffcc","-"),
"hohenlohe":("#bbbbff","-"),
}

blcolordict={
"Baden-Württemberg":("g","-"),
"Bayern":("b","-"),
"Nordrhein-Westphalen":("m","-"),
"Niedersachsen":("#ffff00","-"),
"Hamburg":("r","-"),
}

if plot_todesfall:
	falltext="Tote"
else:
	falltext="Positiv Getestete"

infizierte = cPickle.load(open("datasources/faelle_lkr.rki")) 
todesfaelle = cPickle.load(open("datasources/todesfaelle_lkr.rki")) 
einwohner_lkr=einwohner.loader()

for f in range(1,14):
	figure(f,figsize=(10,6))
	clf()
	grid()
	xlabel("Tage seit Maerz")
	title("Corona " + falltext)

selected=[]
if len(sys.argv)>1:
	selected=sys.argv[1:]	

colorindex=0
if plot_todesfall:
    for lkr in todesfaelle.keys():
	vals = cumsum(todesfaelle[lkr])
	if vals[-1]<10: 
    	    continue
	vmask = where(vals>1)
	tage = take(data.time[:lkrobj.todesfallendtime+1],vmask)[0]
else:
        vals = cumsum(lkrobj.fall[:lkrobj.fallendtime+1])
		if vals[-1]<150: 
			continue
		vmask = where(vals>20)
		tage = take(data.time[:lkrobj.fallendtime+1],vmask)[0]
	if len(tage)<8: 
		continue
	vals = take(vals,vmask)[0]
	"""
	if colordict.has_key(lkr):
		color,ls=colordict[lkr]
		alpha = 0.9
	else:
		#color = colors[i%len(colors)]
		color = "k"
		alpha = 0.3
		ls = "-"
	"""	
	color="k"		
	alpha=0.1
	palpha=0.2
	label=None
	lw=1
	ls="-"

	for sel in selected:
		if sel.lower() in lkrobj.bl.lower():
			label=None
			alpha=0.4
			color,ls=blcolordict.get(lkrobj.bl,("y","-"))
			lw=2

	for sel in selected:
		if sel.lower() in lkr.lower():
			label=lkr.replace("ü","ue")	
			label=label.replace("ö","oe")	
			label=label.replace("ä","ae")	
			label=label.replace("ß","ss")	
			print(label)
			alpha=0.8
			palpha=1
			color=colors[colorindex]
			colorindex+=1
			if colorindex==len(colors):
				colorindex=0
			lw=3
	
	if do_label_all:
			label=lkr.replace("ü","ue")	
			label=label.replace("ö","oe")	
			label=label.replace("ä","ae")	
			label=label.replace("ß","ss")	
			print(label)
			alpha=0.8
			palpha=1
			color=colors[colorindex]
			colorindex+=1
			if colorindex==len(colors):
				colorindex=0
			lw=3
	#label=None

	if lkr in ["stuttgart","berlin","muc","hamburg"]:
		lw = 4
	else:
		lw = 1.5
	figure(1)
	plot(tage,vals,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(2)
	semilogy(tage,vals,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(3)
	slope = vals[1:]/vals[:-1]	
	plot(tage[1:],slope,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	"""
	figure(4)
	slope5=vals[4:]/vals[:-4]
	plot(tage[4:],slope5,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	plot([tage[4],tage[-1]],[1,1],color="k",ls=":")
	"""
	figure(5)
	slope7=vals[7:]/vals[:-7]
	plot(tage[7:],slope7,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	plot([tage[7],tage[-1]],[1,1],color="k",ls=":",alpha=0.2)
	figure(11)
	semilogy(tage[7:],slope7,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(6)
	if einwohner_lkr.has_key(lkr):
                ew = einwohner_lkr[lkr]["ew"]
		plot(tage,vals*1000.0/ew,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
		mask = where(tage<20)
		pretage = take(tage,mask)[0]
		prevals = take(vals,mask)[0]
		preslope7=prevals[7:]/prevals[:-7]
		figure(12)
		loglog(prevals[7:]*1000.0/ew,preslope7,"o",label=label,color=color,alpha=alpha)
		tswitch=20+(len(data.time)-len(tage))
		mask = where(tage>=tswitch)
		posttage = take(tage,mask)[0]
		postvals = take(vals,mask)[0]
		postslope7=postvals[7:]/postvals[:-7]
		loglog(postvals[7:]*1000.0/ew,postslope7,"s",color=color,alpha=alpha)
		loglog(vals[7:]*1000.0/ew,slope7,"+",color=color,alpha=palpha)
		figure(13)
		plot(prevals[7:]*1000.0/ew,preslope7,"o",label=label,color=color,alpha=alpha)
		plot(postvals[7:]*1000.0/ew,postslope7,"s",color=color,alpha=alpha)
		plot(vals[7:]*1000.0/ew,slope7,"+",color=color,alpha=palpha)
	figure(7)
	if einwohner_lkr.has_key(lkr):
		semilogy(tage,vals*1000.0/ew,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(8)
	plot(tage[1:],diff(vals),ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(9)
	if einwohner_lkr.has_key(lkr):
		plot(tage[7:],(vals[7:]-vals[:-7])*1000.0/ew,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(10)
	if einwohner_lkr.has_key(lkr):
		plot(tage[3:],(vals[3:]-vals[:-3])*1000.0/ew,ls=ls,lw=lw,label=label,color=color,alpha=alpha)

infizierte_land = data.bl
"""
for color,(land,vals) in zip(["b","g"],infizierte_land.items()):
	vals = array(vals)
	if einwohner_land.has_key(land):
		color,ls = colordict.get(land,("#333333",":"))	
		pv = polyfit(tage,vals,5)
		vals = array(vals,dtype=float)
		figure(5)
		slope7=vals[7:]/vals[:-7]
		plot(tage[7:],slope7,ls=ls,lw=10,alpha=0.1,label=land,color=color)
		figure(11)
		semilogy(tage[7:],slope7,ls=ls,lw=10,alpha=0.1,label=land,color=color)
		figure(7)
		semilogy(tage,vals*1000.0/einwohner_land[land],ls=ls,lw=10,alpha=0.1,label=land,color=color)
		figure(8)
		plot(tage[1:],diff(vals),ls=ls,lw=10,alpha=0.1,label=land,color=color)
		figure(9)
		if einwohner_land.has_key(land):
			plot(tage[1:],diff(vals)*1000.0/einwohner_land[land],ls=ls,lw=10,alpha=0.1,label=land,color=color)
"""

figure(1)
legend(loc=2)
ylabel(falltext)
xlim(1,plotendx)
savefig("corona %s.png"%falltext)
figure(2)
legend(loc=4)
xlim(xlim()[0],xlim()[-1]+10)
ylabel(falltext)
savefig("coronalog.png")
figure(3)
legend(loc=2)
ylim(0,2)
xlim(1,xlim()[-1])
xlim(1,plotendx)
ylabel("Steigerungsfaktor pro Tag")
savefig("coronasteigerung %s.png"%falltext)
"""
figure(4)
legend(loc=2)
ylim(0,10)
xlim(1,xlim()[-1])
ylabel("Steigerungsfaktor pro 5 Tage")
savefig("coronasteigerung5.png")
"""
figure(5)
legend(loc=2,framealpha=0.8)
#xlim(1,xlim()[-1])
xlim(1,plotendx)
ylim(0,12)
ylabel("Steigerungsfaktor pro Woche")
savefig("coronasteigerung_pro_woche %s.png"%falltext)
figure(6)
xlim(1,plotendx)
legend(loc=2,framealpha=0.8)
ylabel(falltext + " pro 1000 Einwohner")
savefig("conrona_dichte %s.png"%falltext)
figure(7)
xlim(1,plotendx)
legend(loc=2,framealpha=0.8)
ylabel(falltext + " pro 1000 Einwohner")
savefig("conrona_dichte_log %s.png"%falltext)
figure(8)
xlim(1,plotendx)
legend(loc=2,framealpha=0.8)
ylabel("Tgliche Zunahme "+falltext)
savefig("Coronazunahme.png")
figure(9)
xlim(1,plotendx)
legend(loc=2,framealpha=0.8)
ylabel("Wochentliche Zunahme %s/pro 1000 Einwohner"%falltext)
savefig("Coronadichtezunahme %s .png"%falltext)
figure(10)
xlim(1,plotendx)
ylim(0,1.5)
legend(loc=2,framealpha=0.8)
ylabel("3-Tageszunahme %s/pro 1000 Einwohner"%falltext)
savefig("Coronadichtezunahme3tage.png")
figure(11)
xlim(1,plotendx)
legend(loc=2,framealpha=0.8)
#xlim(1,xlim()[-1])
ylim(0,12)
ylabel("Steigerungsfaktor pro Woche")
savefig("coronasteigerung_pro_woche_log %s.png"%falltext)
figure(12)
ylim(1,12)
legend(loc=3,framealpha=0.5)
xlabel(falltext +" / 1000 Einwohner")
ylabel("Steigerungsfaktor pro Woche")
savefig("steigerungsfaktor_vs_infiziertendichte %s.png"%falltext)
figure(13)
ylim(0,12)
legend(loc=1,framealpha=0.5)
xlabel(falltext + "/ 1000 Einwohner")
ylabel("Steigerungsfaktor pro Woche")
savefig("steigerungsfaktor_vs_infiziertendichte_lin %s.png"%falltext)
