#!/usr/bin/python
# vim: set fileencoding=utf8

import pandas
from IPython import embed
import traceback
from pylab import *
from matplotlib import patches
from isoweek import Week
from types import * 
from utils import *

pflegebeduerftige= 4.1e6 
pflegeheimbewohner=800000.0
pflegeheim_coronatote = 19449.0
coronatote_gesamt = 67696.0

resultspath="results/"

def plot_coronatote_pie():
    fig=figure(figsize=(7,7))
    clf()
    pie([pflegeheim_coronatote,pflegeheimbewohner - pflegeheim_coronatote],labels=["Corona-Tote",""]
        ,explode=[0.05,0]
        ,colors=["r","b"])     
    quellenangabe(fig,
                  msg=u"""Daten: RKI Situationsbericht vom 20.2.2021 und Pressemitteilung des Statistischen Bundesamtes vom Dezember 2020
Aufgrund von Testungenauigkeiten und gefälschten Totenscheinen sind die Zahlen mit Vorsicht zu betrachten.""",
                 rotation=0,
                 )                  
    title(u"Anteil an oder mit Corona Verstorbener\n an den Pflegeheimbewohnern")
    savefig(u"%s/Anteil_Coronatote_an_den_Pflegeheimbewohnern.jpg"%resultspath) 
    
    fig=figure(figsize=(7,7))
    clf()
    pie([pflegeheim_coronatote,coronatote_gesamt-pflegeheim_coronatote],
        labels=[u"verstorbene\nPflegeheimbewohner",""],
        explode=[0.05,0],
        colors=["r","#880088"])
    quellenangabe(fig,
                  msg=u"""Daten: RKI Situationsbericht vom 20.2.2021
Aufgrund von Testungenauigkeiten und gefälschten Totenscheinen sind die Zahlen mit Vorsicht zu betrachten.""",
                  autopct="%3.1f%%",
                  rotation=0,
                 )                  
    title(u"Anteil an oder mit Corona verstorbener Pflegeheimbewohner\n an den Coronatoten insgesamt")
    savefig(u"%s/Anteil_verstorbene_Pflegeheimbewohner_an_Coronatoten_gesamt.jpg"%resultspath) 

    fig=figure(figsize=(7,7))
    clf()
    pie([pflegeheimbewohner,pflegebeduerftige-pflegeheimbewohner],
        labels=[u"Pflegeheim-\nbewohner",u"Zuhause gepflegte\nPflegebedürftige"],
        explode=[0.05,0],
        colors=["#FF5575","g"])
    quellenangabe(fig,
                  msg=u"""Daten:Pressemitteilung des Statistischen Bundesamtes vom Dezember 2020""",
                  rotation=0,
                  autopct="%3.1f%%",
                  )                  
    title(u"Pflegebedürftige")
    savefig(u"%s/Pflegebedürftige.jpg"%resultspath) 

if __name__=="__main__":
    plot_coronatote_pie()
