#!/bin/sh
cd datasources
wget https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Projekte_RKI/Nowcasting_Zahlen_csv.csv -O nowcasting.csv
wget https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Testzahlen-gesamt.xlsx?__blob=publicationFile -O Testzahlen-gesamt.xlsx
wget https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Fallzahlen_Kum_Tab.xlsx?__blob=publicationFile  -O  Fallzahlen_Kum_Tab.xlsx
wget https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Fallzahlen_Archiv.xlsx?__blob=publicationFile -O Fallzahlen_Archiv.xlsx 
wget https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Altersverteilung.xlsx?__blob=publicationFile -O Altersverteilung.xlsx 
wget https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Klinische_Aspekte.xlsx?__blob=publicationFile -O Klinische_Aspekte.xlsx 
wget https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Ausbruchsdaten.xlsx?__blob=publicationFile -O Ausbruchsdaten.xlsx 
wget https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Projekte_RKI/COVID-19_Todesfaelle.xlsx?__blob=publicationFile -O COVID-19_Todesfaelle.xlsx
git add *.csv
git add *.xlsx
git ci -m "Datenupdate"

