#!/usr/bin/python
# vim: fileencoding=utf8
import einwohner
from pylab import *
from IPython import embed
import traceback
from plot_nowcasting import str2xpos
import datetime

LINEWIDTH = 3
#startdate=datetime.date(2020,10,1)
#enddate=datetime.date(2020,12,31)

resultspath="results/"

def umlautconv(word):
    word=word.replace("ü","ue")	
    word=word.replace("ö","oe")	
    word=word.replace("ä","ae")	
    word=word.replace("ß","ss")	
    return word

def capitalize(word):
    return word.capitalize()

def beautify(lkr):
    lkr = umlautconv(lkr)
    words=lkr.split()
    if words[0]=="sk":
        words[0] = "Stadtkreis"
    elif words[0]=="lk":
        words[0] = "Landkreis"
    words=map(capitalize,words)
    return " ".join(words)

def quellenangabe(fig):
    msg = """Daten: Robert Koch-Institut (RKI), dl-de/by-2-0: Aufgrund von Testungenauigkeiten, wechselnden Teststrategien,
schwankenden Testzahlen, Mehrfachtestungen und falschen Totenscheinen sind die Zahlen mit Vorsicht zu betrachten."""
    text(0.975,0.5,msg,rotation=90,fontsize=6, verticalalignment="center", transform=fig.transFigure)

def plot_multi(faelle,fallname,names,events=[]):
    try:
        fignames=["summe","inzidenz"]
        for  fname in fignames:
            fig=figure(fname,figsize=(10,6))
            clf()
            grid(1)
        figure("summe")
        ylabel("Summe %s / 100000 Ew"%fallname)
        title("Summe")
        figure("inzidenz")
        if fallname=="Verstorbene":
            ylabel("7-Tage Zunahme %s / 100000 Ew"%fallname)
            title("7-Tage Zunahme %s"%fallname)
        else:
            ylabel("7-Tage Inzidenz %s / 100000 Ew"%fallname)
            title("7-Tage Inzidenz")
    except:
        traceback.print_exc()
        embed()
    for name in names: 
        name=name.lower()
        if not name in faelle:
           print(name + " not found")            
           continue 
        try:
            ew = einwohner_all[name]
            positive = cumsum(faelle[name]["total"][1])*100000.0/ew
            ptimes = faelle[name]["total"][0]
        except:
            traceback.print_exc()
            embed()
        try:
            print("Tote absolut: %s"%cumsum(todesfaelle[name]["total"][1]))
            tote = cumsum(todesfaelle[name]["total"][1])*100000.0/ew
            ttimes = todesfaelle[name]["total"][0]
        except:
            tote=[]
            ttimes=[]
        
        for fname in fignames:     
            figure(fname)
       
        name = beautify(name)
        fig=figure("summe")
        plot(ptimes,positive,lw=LINEWIDTH,label="%s %s"%(fallname,name),alpha=0.7)
        legend(loc=2,framealpha=0.7)
        fig=figure("inzidenz")
        plot(ptimes[6:],positive[6:]-positive[:-6],lw=LINEWIDTH,label="%s %s"%(fallname,name),alpha=0.7)
        ymin,ymax=ylim()

        legend(loc=2,framealpha=0.7)
    for figname in fignames:
        figure(figname)
        quellenangabe(fig)
        ymin,ymax=ylim()
        for event in events:
            try:
                edate = str2xpos(event[0])
                plot([edate,edate],[ymin,ymax],color="k",lw=3,ls="--",alpha=0.4)
                text(edate+datetime.timedelta(1),(ymin+ymax)/2,event[1],rotation=90.0,alpha=0.4
                     ,horizontalalignment="left"
                    ,verticalalignment="center"
                    ,fontsize=10
                    #,transform=fig.transFigure
                    )
            except:
                traceback.print_exc()
                embed()
    figure("summe")
    try: 
        xlim(startdate,enddate)
    except:
        try:
            xlim(startdate,xlim()[-1])
            print("using only startdate")
        except:
            print("auto start and end date")
    title("Kumulierte Summe %s pro 100000 Einwohner"%fallname)
    ylim(0,ylim()[-1])
    if len(names)==1:
        savefig("%s/Corona_Summe_%s_%s.png"%(resultspath,fallname,names[0].replace(" ","_")))
    else:
        savefig("%s/Corona_multi_Summe_%s.png"%(resultspath,fallname))
    close()
    figure("inzidenz")
    try: 
        xlim(startdate,enddate)
    except:
        try:
            xlim(startdate,xlim()[-1])
            print("using only startdate")
        except:
            print("auto start and end date")
    ylim(0,ylim()[-1])
    if fallname=="Verstorbene":
        falltyp="Zunahme_Verstorbene"
    else:
        falltyp="Inzidenz"
    if len(names)==1:
        savefig("%s/Corona_%s_%s.png"%(resultspath,falltyp,names[0]))
    else:
        savefig("%s/Corona_multi_%s.png"%(resultspath,falltyp))
    close()


if __name__=="__main__":
    import sys, cPickle
    names = sys.argv[1:]
    if names[0].lower()=="bl":
        names=einwohner.abk_laender.values()    
    events=[
        ("23.3.2020",u"Bundesweite Kontaktbeschränkungen"),
        ("29.4.2020",u"Bundsweite Maskenpflicht"),
        ("02.Nov.2020",u"Lockdown Light"),
        #("16.Nov.2020",u"Erwarteter Wirkungseintritt Lockdown Light"),
        ("16.dec.2020",u"Verschärfter Lockdown"),
        #("30.dec.2020",u"Erwarteter Wirkungseintritt verschärfter Lockdown"),
        #("16.jan.2021",u"Wandergruppe Mühlheim"),
    ]

    faelle = cPickle.load(open("datasources/faelle.rki"))
    todesfaelle = cPickle.load(open("datasources/todesfaelle.rki"))
    einwohner_all=einwohner.load_all(tolower=True)
    plot_multi(faelle,"Positive",names,events=events,)
    plot_multi(todesfaelle,"Verstorbene",names,events=events)
    #plot_absolut_weekly(faelle,todesfaelle,names)
    #plot_faktor_weekly(faelle,todesfaell,names)
