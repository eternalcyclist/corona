#!/usr/bin/python
# vim: set fileencoding=utf8

import pandas
from IPython import embed
import traceback
from pylab import *
import pylab
from matplotlib import patches
from isoweek import Week
from utils import *
from testzahlen import load_anzahltests


filename = "datasources/Altersverteilung.xlsx"

def quellenangabe(fig):
    msg = """Daten: RKI Altersverteilung.xlsx |Aufgrund von Testungenauigkeiten, wechselnden Teststrategien,
schwankenden Testzahlen, Mehrfachtestungen und falschen Totenscheinen sind die Zahlen mit Vorsicht zu betrachten."""
    pylab.text(0.975,0.5,msg,rotation=90,fontsize=6, verticalalignment="center", transform=fig.transFigure)

def load_data():
    faelle = pandas.read_excel(filename,"Fallzahlen",index_col=0,skiprows=0)
    inzidenzen = pandas.read_excel(filename,"7-Tage-Inzidenzen",index_col=0,skiprows=0,header=None)
    return faelle,inzidenzen

def weekyears2dates(weekyears):
    dates=[]
    for weekyear in weekyears.tolist():
        y,w = map(int,weekyear.split("_"))
        week=Week(y,w)
        dates.append(week.sunday())
    return dates

def plot_verlauf(data,name,events=[],show_gesamt=False,show_alter=True,
                plot_dots=False,
                startdate=None,
                enddate=None,
                ymin=None,
                ymax=None,
                y2min=0,
                y2max=None,
                show_testanzahl=True,
                ):
    fig=figure(figsize=(10,6))
    clf()
    grid(1)
    quellenangabe(fig)
    xticks(rotation=30)
    rows = data.iterrows()
    try:
        header=rows.next()
        dates = weekyears2dates(header[1])
        gesamt=rows.next() # ignore "gesamt"
    except:
        traceback.print_exc()
        embed()
    try:
        if show_alter:
            l = shape(data)[0]-1
            for i,row in enumerate(rows):
                c = cm.jet(1-(i*1.0/l))
                if plot_dots:
                    plot(dates,row[1].tolist(),"-",alpha=0.4,color=c)
                    plot(dates,row[1].tolist(),"o",alpha=0.7,color=c,label=row[1].name)
                else:
                    plot(dates,row[1].tolist(),color=c,alpha=0.7,label=row[1].name)
        if show_gesamt:
                plot(dates,gesamt[1].tolist(),"o-",color="k",lw=2,label="Gesamt")
    except:
        traceback.print_exc()
        embed()
    try: 
        xlim(startdate,enddate)
    except:
        try:
            enddate=max(dates)
            xlim(startdate,enddate)
            print("using only startdate")
        except:
            traceback.print_exc()
            print("auto start and end date")
            startdate=min(dates)
            enddate=max(dates)
            xlim(startdate,enddate)
    if ymax==None:
        if ymin==None:
            ymin,ymax=ylim()
    else:
        if ymin==None:
            ymin=ylim()[0]
    for event in events:
        try:
            edate = str2xpos(event[0])
            plot([edate,edate],[ymin,ymax],color="k",lw=3,ls="--",alpha=0.4)
            text(edate+datetime.timedelta(1),(ymin+ymax)/2,event[1],rotation=90.0,alpha=0.4
                 ,horizontalalignment="left"
                ,verticalalignment="bottom"
                ,fontsize=10
                #,transform=fig.transFigure
                )
        except:
            traceback.print_exc()
            embed()
    tcdate=datetime.date(2020,11,3)
    if startdate==None or startdate<=tcdate:
        bar(tcdate,ymax,1e99,color="r", alpha=0.075,align="edge")
        text(datetime.date(2020,11,7),ymax,"Andere Testkriterien",color="r",alpha=0.4,rotation=0,fontsize=10,horizontalalignment="left",verticalalignment="top")
    ylim(ymin,ymax)
    legend(loc=2)
    ylabel(name)
    title(name)
    testdates,testanzahl=load_anzahltests()
    if show_testanzahl:
        twinx()
        plot(testdates,testanzahl,"r",alpha=0.2,lw=12,label="Anzahl Tests")
        legend(loc=1)
        if y2max==None:
            ymin,ymax=ylim()
        else:
            ymin=ylim()[0]
    ylim(y2min,y2max)
    savefig("results/Corona_%s.png"%name.replace(" ","_"))

if __name__=="__main__":
    faelle,inzidenzen=load_data()
    events=[
        ("23.3.2020",u"Bundesweite Kontaktbeschränkungen"),
        ("29.4.2020",u"Bundsweite Maskenpflicht"),
        ("02.Nov.2020",u"Lockdown Light"),
        #("03.Nov.2020",u"Änderung der Testkriterien"),
        #("16.Nov.2020",u"Erwarteter Wirkungseintritt Lockdown Light"),
        ("16.dec.2020",u"Verschärfter Lockdown"),
        #("30.dec.2020",u"Erwarteter Wirkungseintritt verschärfter Lockdown"),
        #("16.jan.2021",u"Wandergruppe Mühlheim"),
    ]

    #plot_verlauf(faelle,u"Fälle")
    #datetime.date(2020,12,31)
    plot_verlauf(inzidenzen,u"Alterspezifische Inzidenz Start 2.Welle",
                 startdate=datetime.date(2020,10,1),
                 enddate=datetime.date(2020,12,1),
                 plot_dots=True,
                 events=events,
                ymax=500)
    plot_verlauf(inzidenzen,u"Alterspezifische Inzidenz in Deutschland",events=events)
    plot_verlauf(inzidenzen,u"Bundesweite Inzidenz",events=events,show_alter=False,show_gesamt=True)

    events=[
        #("23.3.2020",u"Bundesweite Kontaktbeschränkungen"),
        #("29.4.2020",u"Bundsweite Maskenpflicht"),
        ("02.Nov.2020",u"Lockdown Light"),
        #("03.Nov.2020",u"Änderung der Testkriterien"),
        #("16.Nov.2020",u"Erwarteter Wirkungseintritt Lockdown Light"),
        ("16.dec.2020",u"Verschärfter Lockdown"),
        #("30.dec.2020",u"Erwarteter Wirkungseintritt verschärfter Lockdown"),
        #("16.jan.2021",u"Wandergruppe Mühlheim"),
    ]
    plot_verlauf(inzidenzen,u"Alterspezifische Inzidenz in Deutschland - Sommerloch",
                 startdate=datetime.date(2020,5,1),
                 enddate=datetime.date(2020,9,30),
                 plot_dots=True,
                 events=events,
                 ymax=38,
                 y2min=200000,
                 y2max=1550000)

    plot_verlauf(inzidenzen,u"Alterspezifische Inzidenz in Deutschland - Frühjahr 2021",
                 startdate=datetime.date(2021,2,1),
                 enddate=datetime.date(2021,3,1),
                 plot_dots=True,
                 events=events,
                 ymax=100,
                 ymin=25,
                 y2min=1025000,
                 y2max=1200000)
    
