# vim: set fileencoding=utf8

import pandas
import pylab
import re
from numpy import zeros
from IPython import embed
import traceback

def str2xpos(str):
    return pandas._libs.tslibs.parsing.parse_datetime_string(str)

def quellenangabe(fig):
    msg = """Daten: Robert Koch-Institut (RKI), dl-de/by-2-0.| Aufgrund von Testungenauigkeiten, wechselnden Teststrategien,
schwankenden Testzahlen, Mehrfachtestungen und falschen Totenscheinen sind die Zahlen mit Vorsicht zu betrachten."""
    pylab.text(0.975,0.5,msg,rotation=90,fontsize=6, verticalalignment="center", transform=fig.transFigure)


def quellenangabe(fig,
        msg = u"""Daten: RKI, COVID-19-Todesfälle.xlsx nach Sterbedatum, |Aufgrund von Testungenauigkeiten, wechselnden Teststrategien,
schwankenden Testzahlen, Mehrfachtestungen und falschen Totenscheinen sind die Zahlen mit Vorsicht zu betrachten.""",
        rotation=90,
        ):

    if rotation==90:
        xpos,ypos=0.975,0.5
        valign="center"
        halign="center"
    else:
        xpos,ypos=0.5,0
        valign="bottom"
        halign="center"
    pylab.text(xpos,ypos,msg,rotation=rotation,fontsize=6, verticalalignment=valign, horizontalalignment=halign, transform=fig.transFigure)

def agstr2range(str,inclusive=False):
    """
    inclusive - ob das obere Limit im String inklusive ist
    return: 
        Es wird immer mit oberem Limit exklusive returned
    """
    try:
        print("WARNIMG: agstr2range: Manchmal sind upper limits exclusive und manchmal inclusive")
        fnd = re.match("\D*(\d*)\D*(\d)",str)  
        if fnd==None:
            return None
        valstrs=fnd.groups()
    except:
        traceback.print_exc()
        embed()
    try:
        start = int(valstrs[0])
    except:
        start = None
    try:
        end = int(valstrs[1])
    except:
        end = None
    if "unter" in str:
        start = 0
    if "mehr" in str:
        end = 999
    if u"über" in str:
        end = 999
    if "+" in str:
        end = 999
    return (start,end)

def agregroup(outags,agdict):
    outagdict={}

    def findag(agtuple):
        if agtuple[0]==None:
            agtuple[0]=agtuple[1]-1
        if agtuple[1]==None:
            agtuple[1]=agtuple[0]+1
        lastag=0
        for outag in outags:
            if outag==999:
                if agtuple[0]>=lastag and agtuple[1]>=lastag:
                    return outag    

            if agtuple[1]<outag and agtuple[0]<outag and agtuple[0]>=lastag and agtuple[1]>=lastag:
                return outag    
        
    for outag in outags:
        outagdict[outag]=zeros(len(agdict.values()[0]))    
    try:
        for agstr in agdict.keys():
            agr = agstr2range(agstr)
            if agr==None:
                print("Warning: %s not usable. Ignoring it."%agstr)
                continue
            outag=findag(agr)
            print("%s %s")%(agstr,agr)
            outagdict[outag]+=agdict[agstr]
    except:
        traceback.print_exc()
        embed()
    return outagdict

def agresample(outags,agdict):
    ags = agdict.keys()
    ags.sort()
    
    for aglow,aghigh in zip([0].extend(ags),ags.append(100)):
        print(aglow,aghigh)
    print("Noch nicht fertig!!")     


def agname(i,agbins):
    if i==0:
        return "unter %i"%agbins[0]
    if i==len(agbins)-1 or agbins[i]==999:
        return u"%i und älter"%agbins[i-1]
    else:
        return "%i..%i"%(agbins[i-1],agbins[i])

