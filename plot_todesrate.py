#!/usr/bin/python
#vim: fileencoding=utf8

from pylab import *
import rki
import einwohner
import sys

colors = ["r","b","g","y","m","c","#ffaa00","#00ffaa","#0055ff","#f03af3","#44ff00"]

endtime = 50

def plot_todesrate():
	colorindex=0
	lw=2
	alpha=0.7
	figure(figsize=(10,6))
	xlabel("Tage")
	grid()
	data = rki.loader(endtime=endtime)
	einwohner_lkr = einwohner.loader()
	for lkr,lkrobj in data.lkr.items():
		ende = lkrobj.fallendtime
		infizierte=cumsum(lkrobj.fall[:ende])
		tote = cumsum(lkrobj.todesfall[:ende])
		if tote[-1]<10: 
			continue
		mask = where(tote>3)
		tote = take(tote,mask)[0]
		infizierte = take(infizierte,mask)[0]
		tage = take(data.time[:ende],mask)[0]
		if (len(tage)<6):
			continue
		if (tote[-1]<6):
			continue
		label=lkr.replace("ü","ue")
		label=label.replace("ö","oe")	
		label=label.replace("ä","ae")	
		label=label.replace("ß","ss")	
		color = colors[colorindex]
		colorindex += 1
		if colorindex==len(colors):
			colorindex=0
		plot(tage,tote/infizierte*100,lw=lw,color=color,label=label,alpha=alpha)
		#plot(tage[5:],tote[5:]/infizierte[:-5],ls="--")
	legend(loc=2,framealpha=0.4)	
	ylim(0,20)
	ylabel("Tote pro positiv Geteste / Prozent")
	savefig("todesrate.png")


if __name__=="__main__":
	plot_todesrate()



