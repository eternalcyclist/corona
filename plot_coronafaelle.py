#!/usr/bin/python
# vim:fileencoding=utf8


import rki
import IPython
import scipy.optimize as so
import datetime
import traceback
import pandas
import einwohner
from pylab import *


#altercolors={"A00-A04":"b","A05-A34":"c","A15-A34":"tab:pink","A35-A59":"g","A60-A79":"tab:orange","A80+":"r","total":"k"}
altercolors={04:"b",15:"c",34:"tab:pink",59:"g",79:"tab:orange",999:"r","total":"k"}

def date_extrapolate(xin,yin,istart,iend,ostart,oend):
    """ extrapolyte yin
    xin is datetime arrrays
    istart,iend,ostart,oend are start and end dates of relevant input data and output data
    """
    try:
        startidx=None
        for i,d in enumerate(xin):
            if startidx==None:
                if d>=istart:
                    startidx=i
        delta_in=iend - istart
        endidx = startidx + delta_in.days
        x=arange(1.0*startidx,endidx+1)
        y=zeros(len(x))
        dstart=xin[startidx]
        for d,yi in zip(xin,yin):
            td = d-dstart
            if td.days>=len(y):
                break
            if td.days<0:
                continue
            y[td.days]=yi
    except:
        traceback.print_exc()
        IPython.embed()
            
    def exp_fun(x,a,b,o):
        return o+a*(x)**b

    print("vor curvefit")
    popt,dummy = so.curve_fit(exp_fun,x,y) #,p0=(250000,-2,0))
    cdelta=oend-istart
    xc=arange(cdelta.days)
    yc=exp_fun(xc,*popt)
    odelta=oend-ostart
    xout=array([ostart+datetime.timedelta(i) for i in range(odelta.days)])
    sd = ostart-istart
    yout = yc[sd.days:]
    #print("extrapolate")
    #IPython.embed()
    return xout,yout

def plot_laender(data,alter="total",relativ=False,which="all"):
    if relativ:
        ewl = einwohner.load_laender()
        relstr = "_rel"
    else:
        relstr = ""
    bls=data.bl.keys()
    bls.sort()
    figure("bl sum",figsize=(10,6))
    clf()
    grid()
    figure("bl",figsize=(10,6))
    clf()
    grid()
    div=1
    for bl in bls:
        if which.lower()!="all":
            if bl.lower()!=which.lower():
                continue
        if relativ:
            try:
                div = ewl[bl][2018] * 0.001
            except:
                traceback.print_exc()
                IPython.embed()
        items = data.bl[bl].todesfall[alter].items()
        items.sort()
        bl = bl.decode("ascii","ignore")
        xy = array(items)
        figure("bl")
        plot(xy[:,0],xy[:,1]/div,label=bl)
        figure("bl sum")
        plot(xy[:,0],cumsum(xy[:,1])/div,label=bl)
    legend(loc=2)
    if relativ:
        ylabel("Summe der Sterbefaelle / ppm")
    else:
        ylabel("Summe der Sterbefaelle")
    title("Summe der Coronasterbefaelle %s"%alter)
    savefig("Coronatote_laender_summe_%s%s%s.png"%(alter,alter,relstr))
    figure("bl")
    legend(loc=2)
    if relativ:
        ylabel("Sterbefaelle pro Tag / ppm")
    else:
        ylabel("Sterbefaelle pro Tag")
    title("Taegliche Coronasterbefaelle %s"%alter)
    savefig("Coronatote_laender_pro_tag_%s_%s%s.png"%(alter,which,relstr))

def plot_todesfaelle(data,which="Deutschland",do_extrapolate=True):
    which=which.lower()
    if which=="deutschland":
        d = data.gesamt
    else:
        for bl in data.bl.keys():
            if which in bl:
                which=bl
                break
        if which in data.bl:
            d = data.bl[which]
        else:
            for lkr in data.lkr.keys():
                if which in lkr:
                    which=lkr
                    break
            d = data.lkr[which]
    which=which.decode("ascii","ignore")
    alters=d.todesfall.keys()
    alters.sort()
    figure(which+" sum")
    grid()
    figure(which)
    grid()
    for alter in alters:
        color = altercolors[alter]
        items=d.todesfall[alter].items()
        items.sort()
        xy = array(items)
        figure(which)
        plot(xy[:,0],xy[:,1],label=alter,color=color)
        if do_extrapolate:
            if len(xy[:,0])>10:
                try:
                    xex,yex=date_extrapolate(xy[:,0],xy[:,1],xy[0,0],datetime.date(2020,3,26),xy[0,0],datetime.date(2020,4,16))
                    #lxex,yex=date_extrapolate(xy[:,0],xy[:,1],datetime.date(2020,4,2),datetime.date(2020,4,16),datetime.date(2020,4,2),xy[-1,0])
                    plot(xex,yex,ls=":",color=color)
                    outdf=pandas.DataFrame(data=[yex],columns=xex)
                    outdf.to_csv("corona_extrapolation.dat")
                except RuntimeError:
                    traceback.print_exc()
                    yex=xex=[]
        figure(which+" sum")
        plot(xy[:,0],cumsum(xy[:,1]),label=alter,color=color)
        if do_extrapolate:
            if len(xy[:,0])>10:
                plot(xex,cumsum(yex),ls=":",color=color)
    legend(loc=2)
    ylabel("Summe der Sterbefaelle")
    title("Summe der Coronasterbefaelle in %s"%which.capitalize())
    savefig("Coronatote_summe_%s.png"%which)
    figure(which)
    legend(loc=2)
    ylabel("Sterbefaelle pro Tag")
    title("Taegliche Coronasterbefaelle in %s"%which.capitalize())
    savefig("Coronatote_pro_tag_%s.png"%which)

def plot_faelle(data,which="Deutschland",do_extrapolate=True):
    which=which.lower()
    if which=="deutschland":
        d = data.gesamt
    else:
        for bl in data.bl.keys():
            if which in bl:
                which=bl
                break
        if which in data.bl:
            d = data.bl[which]
        else:
            for lkr in data.lkr.keys():
                if which in lkr:
                    which=lkr
                    break
            d = data.lkr[which]
    which=which.decode("ascii","ignore")
    alters=d.todesfall.keys()
    alters.sort()
    figure(which+" sum",figsize=(10,6))
    clf()
    grid()
    figure(which,figsize=(10,6))
    clf()
    grid()
    for alter in alters:
        color = altercolors[alter]
        items=d.fall[alter].items()
        items.sort()
        xy = array(items)
        figure(which)
        plot(xy[:,0],xy[:,1],label=alter,color=color)
        if do_extrapolate:
            if len(xy[:,0])>10:
                try:
                    xex,yex=date_extrapolate(xy[:,0],xy[:,1],xy[0,0],datetime.date(2020,3,21),xy[0,0],datetime.date(2020,4,16))
                    #lxex,yex=date_extrapolate(xy[:,0],xy[:,1],datetime.date(2020,4,2),datetime.date(2020,4,16),datetime.date(2020,4,2),xy[-1,0])
                    plot(xex,yex,ls=":",color=color)
                    outdf=pandas.DataFrame(data=[yex],columns=xex)
                    outdf.to_csv("corona_extrapolation_confirmed.dat")
                except RuntimeError:
                    traceback.print_exc()
                    yex=xex=[]
        figure(which+" sum")
        plot(xy[:,0],cumsum(xy[:,1]),label=alter,color=color)
        if do_extrapolate:
            if len(xy[:,0])>10:
                plot(xex,cumsum(yex),ls=":",color=color)
    xticks(rotation=60)
    legend(loc=2)
    ylabel("Summe der positiv Getesteten")
    title("Summe der positiv Getesteten in %s"%which.capitalize())
    savefig("Coronapositive_summe_%s.png"%which)
    figure(which)
    legend(loc=2)
    xticks(rotation=30)
    ylabel("Positiv Getestete pro Tag")
    title("Taegliche positiv auf Corona Getestete %s"%which.capitalize())
    savefig("Coronapositive_pro_tag_%s.png"%which)
    ion()
    show()

if __name__=="__main__":    
    import sys
    data = rki.loader()
    if len(sys.argv)==1:
        plot_todesfaelle(data,do_extrapolate=False)
        plot_faelle(data,do_extrapolate=False)
        plot_laender(data,relativ=True,do_extrapolate=False)
        for land in ("Thüringen","Bayern","Baden-Württemberg"):
            plot_laender(data,which=land)
            plot_laender(data,which=land,relativ=True)
    else:
        for which in sys.argv[1:]:
            plot_todesfaelle(data,which=which,do_extrapolate=False)
            plot_faelle(data,which=which,do_extrapolate=False)
