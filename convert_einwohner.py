#!/usr/bin/python

def convert(filename):
	outf = open("landkreise.dat","w")
	inf = open(filename)
	while True:
		line = inf.readline()
		if line.startswith("KrS"):
			break
	for line in inf.readlines():
		line = line.strip()
		words = line.split()
		if len(words)<7:
			continue
		print(words)
		ewstr = words[-4]
		ewstr = ewstr.replace(".","")
		if not ewstr.isdigit():
			print("Falsche Einwohnerzahl: %s"%ewstr)
			continue
		kreisname = words[1]
		kreisname = kreisname.split(",")[0]
		kreisname = kreisname.split("[")[0]
		if kreisname == "Bad":
			kreisname += " " + words[2]
		ewstr = ewstr.replace(".","")
		einwohner = int(ewstr)
		outf.write("%s\t%i\n"%(kreisname,einwohner))	

if __name__=="__main__":
	import sys
	convert(sys.argv[1])


