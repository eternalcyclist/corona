#!/usr/bin/python
# vim: fileencoding=utf8

from IPython import embed
import pandas
import traceback
from pylab import *

filename = "datasources/nowcasting.csv"

def str2xpos(str):
    return pandas._libs.tslibs.parsing.parse_datetime_string(str)

def load():
    df = pandas.read_csv(filename,decimal=",",sep=";",skiprows=5, skipfooter=19, parse_dates=[0], dayfirst=True)
    return df

def plot_rval(df,events=[]):
    fig=figure(figsize=(10,5))
    clf()
    grid()
    #plot(df.values[:,0],df.values[:,-6])   # 4 Tage
    plot(df.values[:,0],df.values[:,-3],lw=3)
    #plot(df.values[:,0],df.values[:,-2])
    #plot(df.values[:,0],df.values[:,-1])
    ymin,ymax=ylim()
    for event in events:
        try:
            #bar(event.date,height=ylim()[-1],color="k")
            edate = str2xpos(event[0])
            plot([edate,edate],[ymin,ymax],color="k",lw=3,ls="--",alpha=0.5)
            text(edate+datetime.timedelta(1),(ymin+ymax)/2,event[1],rotation=90.0,alpha=0.8
                 ,horizontalalignment="left"
                ,verticalalignment="center"
                ,fontsize=13)
        except:
            traceback.print_exc()
            embed()
    ylabel(u"Reproduktionszahl")
    title(u"Vom  RKI geschätzte über 7 Tagen gemittelte Reproduktionszahl")
    savefig("Corona-Reproduktionszahl-Maskenpflicht.png")
    #savefig("Corona-Reproduktionszahl-Lockdown.png")
    #savefig("Corona-Reproduktionszahl.png")

if __name__=="__main__":
    df = load()
    events=[
        #("23.3.2020",u"Bundesweite Kontaktbeschränkungen"),
        ("29.4.2020",u"Bundsweite Maskenpflicht"),
        #("02.Nov.2020",u"Lockdown Light"),
        #("16.dec.2020",u"Verschärfter Lockdown"),
    ]
    plot_rval(df,events)

