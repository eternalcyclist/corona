#!/usr/bin/python
"""
vim: fileencoding=latin1
"""
import sys
import traceback
from pylab import *

colors = ["r","b","g","y","m","c","#ffaaaa","#aaffaa","#aaaaff","#e033e3","#aaf0aa"]

def loader(filename):
	data = {} 
	data["kws"]=[]
	inf = open(filename)
	dataname = inf.readline().replace("\x00","")
	headerstr = inf.readline().replace("\x00","")
	headerstr = headerstr.replace('"\t"',",")	
	headerstr = headerstr.replace('"',"")
	header = headerstr.split(",")[1:]
	for lkr in header:
		data[lkr]=[]
	for line in inf.readlines():
		line = line.replace("\x00","")
		if len(line)<10:
				continue
		line = line.replace('""',"0")
		line = line.replace(',','.')
		#line = line.replace('"\t"',',')		   
		line = line.replace('"','')
		words = line.split("\t")
		data["kws"].append(int(words[0].split("W")[-1]))
		vals = list(map(float,words[1:]))	
		for lkr,val in zip(header,vals):
			data[lkr].append(val)	
	return data

data = loader(sys.argv[1])

for f in range(1,8):
	figure(f,figsize=(10,6))
	clf()
	grid()
	xlabel("Kalenderwoche")

selected=[]
if len(sys.argv)>2:
	selected=sys.argv[2:]	

colorindex=0
weeks = data["kws"]
for lkr,vals in data.items():
	lkr = lkr.decode("latin-1")
	for sel in selected:
		if sel.lower() in lkr.lower(): 
			color = colors[colorindex]
			colorindex+=1
			if colorindex==len(colors):
				colorindex=0
			alpha=0.6
			ls = "-"
			lw = 3
			label = lkr
			print("found")
			print(lkr)
			print(vals)
			break
		else:
			color = "k"
			alpha = 0.1
			ls = "-"
			lw = 1
			label = None 
	vals = array(vals,dtype=float)/100
	if len(vals)==0:
		print("%s hat 0 daten"%lkr)
		continue
	sumvals=cumsum(vals)
	figure(1)
	plot(weeks,vals,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(2)
	semilogy(weeks,vals,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(3)
	plot(weeks,sumvals,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(4)
	semilogy(weeks,sumvals,ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(5)
	plot(sumvals[1:]/sumvals[:-1],ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	figure(6)
	try:
		semilogy(sumvals[1:]/sumvals[:-1],ls=ls,lw=lw,label=label,color=color,alpha=alpha)
	except:
		pass
figure(1)
legend(loc=2)
ylabel("Neu infiziert Gemeldete/1k")
savefig("s_corona_zunahme_weekly.png")

figure(2)
legend(loc=2)
ylabel("Neu infiziert Gemeldete/1k")
savefig("s_corona_zunahme_weekly_log.png")

figure(3)
legend(loc=2)
ylabel("Summe infiziert Gemeldeter/1k")
savefig("s_corona_summe_weekly.png")

figure(4)
legend(loc=2)
ylabel("Summe infiziert Gemeldeter/1k")
savefig("s_corona_summe_weekly_log.png")
figure(5)
ylim(0,20)
legend(loc=2)
ylabel("Steigerungsfaktor/Woche")
savefig("s_corona_steigerung_pro_woche.png")
figure(6)
legend(loc=2)
ylabel("Steigerungsfaktor/Woche")
savefig("s_corona_steigerung_pro_woche_log.png")
