#!/usr/bin/python
# vim: set fileencoding=utf8

import pandas
from IPython import embed
import traceback
from pylab import *
import pylab
from matplotlib import patches
from isoweek import Week
from utils import *

filename = "datasources/12411-0005.xlsx"

"""
def interpolate_weeks(dates,arr):
    kwdates=
    for row in arr:
"""

def load(dateoffset=True,bins=None,weeks=False,startdate=None):
    df = pandas.read_excel(filename,skiprows=range(5),skipfooter=5)
    dates = map(pandas._libs.tslibs.parsing.parse_datetime_string,df.values[:,0])
    if dateoffset:
        def ycorr(x):
            return x - datetime.timedelta(365)
        dates = map(ycorr,dates)
    if startdate!=None:
        try:
            startdate=datetime.datetime(startdate,12,31)
        except:
            pass 
        startidx=dates.index(startdate)
    else:
        startidx=0
    ges=df.values[startidx:,-1]
    ag1=df.values[startidx:,1:-1]
    if weeks==True:
        ndates,ges = interpolate_weeks(dates,[ges])[0]
        ndates,ag1 = interpolate_weeks(dates,ag1)
    if bins==None:
        return dates,ges,ag1
    else:
        agtemp=gruppierung(ag1,bins)
        agb={}
        for ag,agv in zip(bins,agtemp):
            agb[ag]=agv
        return dates,agb

def gruppierung(ag1,bins):
    """
    bins -- Liste der oberen Enden des Bins
    """
    ag1=ag1.swapaxes(0,1)
    ago=[]
    lastbin=0
    for bin in bins:
        v = zeros(len(ag1[0]))
        print("vz: %s"%v[0])
        try:
            for ix in range(lastbin,bin):
                if ix >= len(ag1):
                    print("ix %i > len(ag1) %i"%(ix,len(ag1)))
                    break
                print(v[0])
                v = v + ag1[ix]
                print(v[0],ag1[ix][0])
        except:
            traceback.print_exc()
            embed()
        print(bin,v[0])
        vo=copy(v)
        ago.append(vo)
        lastbin=bin
        #if sum(v)>0:
        #    if len(ag1[0]==0) or lastbin==-1:
        #        ago.append(v)
        #    else:
        #        for i,u in enumerate(v):
        #            ago[-1][i]+=u
        # TODO: Extrapolation
        #    lastbin=bin
        #else:
        #    break
    return array(ago)

def plot_bevoelkerung_rebinned(startdate=None,enddate=None,bins=[45,65,85,999]):
    dates,ags=load(bins=bins)
    fig = figure(figsize=(10,6))
    clf()
    grid(1)
    keys=ags.keys()
    keys.sort()
    for i,ag in enumerate(keys):
        c=cm.jet((i+1)*1.0/len(keys))
        v=ags[ag]
        try:
            plot(dates,v,color=c,alpha=0.7,label=agname(i,keys))
        except:
            traceback.print_exc()
            embed()
    xmin,xmax=min(dates),max(dates)
    if startdate!=None:
        xmin=startdate
    if enddate!=None:
        xmax=enddate
    xlim(xmin,xmax)
    legend(loc=2)
    ylabel(u"Bevölkerung Altersgruppen")
    title(u"Bevölkerung in Deutschland Altersgruppen")
    quellenangabe(fig,"Daten: Statistisches Bundesamt")
    savefig("results/Bevoelkerung_Deutschland_Altersgruppen_rebinned.jpg")

def plot_bevoelkerung(startdate=None,enddate=None):
    dates,ges,ag1=load()
    fig = figure(figsize=(10,6))
    grid(1)
    plot(dates,ges,lw=3)
    xmin,xmax=min(dates),max(dates)
    if startdate!=None:
        xmin=startdate
    if enddate!=None:
        xmax=enddate
    xlim(xmin,xmax)
    ylim(75e6,85e6)
    ylabel(u"Bevölkerung insgesamt")
    title(u"Bevölkerung in Deutschland insgesamt")
    quellenangabe(fig,"Daten: Statistisches Bundesamt")
    savefig("results/Bevoelkerung_Deutschland_insgesamt.jpg")

    fig = figure(figsize=(10,6))
    clf()
    grid(1)
    agt=ag1.swapaxes(0,1)
    for i,v in enumerate(agt):
        c=cm.jet((i+1)*1.0/len(agt))
        try:
            plot(dates,v,color=c,alpha=0.7)
        except:
            traceback.print_exc()
            embed()
    xmin,xmax=min(dates),max(dates)
    if startdate!=None:
        xmin=startdate
    if enddate!=None:
        xmax=enddate
    xlim(xmin,xmax)
    ylabel(u"Bevölkerung Altersgruppen 1j")
    title(u"Bevölkerung in Deutschland Altersgruppen 1j")
    quellenangabe(fig,"Daten: Statistisches Bundesamt")
    savefig("results/Bevoelkerung_Deutschland_Altersgruppen_1j.jpg")

    agbins=range(5,90,5)
    agbins.append(999)
    ag5 = gruppierung(ag1,agbins)
    fig = figure(figsize=(10,6))
    clf()
    grid(1)
    #agt=ag5.swapaxes(0,1)
    for i,v in enumerate(ag5):
        c=cm.jet((i+1)*1.0/len(ag5))
        try:
            plot(dates,v,"-o",color=c,alpha=0.7,label=agname(i,agbins))
        except:
            traceback.print_exc()
            embed()
    legend(loc=2)
    xmin,xmax=min(dates),max(dates)
    if startdate!=None:
        xmin=startdate
    if enddate!=None:
        xmax=enddate
    xlim(xmin,xmax)
    ylabel(u"Bevölkerung Altersgruppen 5j")
    title(u"Bevölkerung in Deutschland Altersgruppen 5j")
    quellenangabe(fig,"Daten: Statistisches Bundesamt")
    savefig("results/Bevoelkerung_Deutschland_Altersgruppen_5j.jpg")

    fig = figure(figsize=(10,6))
    clf()
    grid(1)
    #agt=ag5.swapaxes(0,1)
    for i,v in enumerate(ag5):
        c=cm.jet((i+1)*1.0/len(ag5))
        try:
            plot(dates[1:],diff(v)*100./v[:-1],"-o",color=c,alpha=0.7,label=agname(i,agbins))
        except:
            traceback.print_exc()
            embed()
    legend(loc=2)
    xmin,xmax=min(dates),max(dates)
    if startdate!=None:
        xmin=startdate
    if enddate!=None:
        xmax=enddate
    xlim(xmin,xmax)
    ylabel(u"Steigerung in Prozentz")
    title(u"Steigerung der Bevölkerung in Deutschland Altersgruppen 5j")
    quellenangabe(fig,"Daten: Statistisches Bundesamt")
    savefig("results/Bevoelkerung_Deutschland_Altersgruppen_5j_Steigerung.jpg")


    agbins=range(5,85,5)
    agbins.append(999)
    ag5 = gruppierung(ag1,agbins)
    fig = figure(figsize=(10,6))
    clf()
    grid(1)
    #agt=ag5.swapaxes(0,1)
    for i,v in enumerate(ag5):
        c=cm.jet((i+1)*1.0/len(ag5))
        try:
            plot(dates,v,"-o",color=c,alpha=0.7,label=agname(i,agbins))
        except:
            traceback.print_exc()
            embed()
    legend(loc=2)
    xmin,xmax=min(dates),max(dates)
    if startdate!=None:
        xmin=startdate
    if enddate!=None:
        xmax=enddate
    xlim(xmin,xmax)
    ylabel(u"Bevölkerung Altersgruppen 5j")
    title(u"Bevölkerung in Deutschland Altersgruppen 5j")
    quellenangabe(fig,"Daten: Statistisches Bundesamt")
    savefig("results/Bevoelkerung_Deutschland_Altersgruppen_5jx.jpg")

    fig = figure(figsize=(10,6))
    clf()
    grid(1)
    #agt=ag5.swapaxes(0,1)
    for i,v in enumerate(ag5):
        c=cm.jet((i+1)*1.0/len(ag5))
        try:
            plot(dates[1:],diff(v)*100./v[:-1],"-o",color=c,alpha=0.7,label=agname(i,agbins))
        except:
            traceback.print_exc()
            embed()
    legend(loc=2)
    xmin,xmax=min(dates),max(dates)
    if startdate!=None:
        xmin=startdate
    if enddate!=None:
        xmax=enddate
    xlim(xmin,xmax)
    ylabel(u"Steigerung in Prozentz")
    title(u"Steigerung der Bevölkerung in Deutschland Altersgruppen 5j")
    quellenangabe(fig,"Daten: Statistisches Bundesamt")
    savefig("results/Bevoelkerung_Deutschland_Altersgruppen_5jx_Steigerung.jpg")



if __name__=="__main__":
    #plot_bevoelkerung(startdate=datetime.date(1990,12,31))
    plot_bevoelkerung(startdate=datetime.date(2011,12,31))
    #plot_bevoelkerung_rebinned(startdate=datetime.date(2011,12,31))
    plot_bevoelkerung_rebinned(startdate=datetime.date(2011,12,31),bins=[60,80,999])
