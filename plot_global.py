#!/usr/bin/python
# vim: fileencoding=utf8

STARTDATE = "3/1/2020"
#STARTDATE = "10/1/2020"
ENDDATE = "3/15/2021"
LINEWIDTH = 6
avglen = 7

import pandas
import IPython
from IPython import embed
from pylab import *
import einwohner
import copy
import traceback
from utils import quellenangabe

def str2xpos(str):
    return pandas._libs.tslibs.parsing.parse_datetime_string(str)

def loader(name,filepath="~/COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_%s_global.csv"):
    
    filename = filepath%name

    class data:
        name = None 
        dates= None
        countries={}
    
    data.name=name
    indata = pandas.read_csv(filename)
    data.dates = [str2xpos(d) for d in  indata.columns[4:]]
    for line in indata.values:
        if type(line[0])!=float:   # keine Kolonien
            continue
        country = line[1]
        values = line[4:] * 1.0
        data.countries[country]=values
    return data

def plot_events(events):

    ymin,ymax=0,ylim()[-1]
    for event in events:
        try:
            #bar(event.date,height=ylim()[-1],color="k")
            edate = str2xpos(event[0])
            plot([edate,edate],[ymin,ymax],color="k",lw=3,ls="--",alpha=0.5)
            text(edate+datetime.timedelta(1),(ymin+ymax)/2,event[1],rotation=90.0,alpha=0.8
                 ,horizontalalignment="left"
                ,verticalalignment="center"
                ,fontsize=13)
        except:
            traceback.print_exc()
            embed()

def plot_countries(data,minval=0,selection=[],name="",ymin=0,ymax=None,titletext="",y_label=None,events=[]):
    print(selection)
    figure(name+"slope",figsize=(10,6))
    clf()
    grid() 
    figure(name+"daily",figsize=(10,6))
    clf()
    grid() 
    figure(name,figsize=(10,6))
    #fig,ax=subplots()
    clf()
    grid() 
    for country,vals in data.countries.items():
        if max(vals)<minval:
            continue
        if len(selection)>0:
            if country not in selection:
                continue
        figure(name)
        plot(data.dates,vals,label=country)
        figure(name+"daily")
        #plot(data.dates[1:],diff(vals),label=country)
        plot(data.dates[avglen:],(vals[avglen:]-vals[:-avglen])*1./avglen,label=country,lw=LINEWIDTH)
        figure(name+"slope")
        delta=(vals[avglen:]-vals[:-avglen])*1./avglen
        delta=where(delta>0,delta,1e-99)
        slope = delta[avglen:]/delta[:-avglen]
        slope=where(slope>1e90,0,slope)
        plot(data.dates[2*avglen:],slope,label=country,lw=LINEWIDTH)

    fig=figure(name)
    if ymax==None:
        ymax=ylim()[-1]

    ylim(ymin,ymax)
    plot_events(events)    
    xlim(str2xpos(STARTDATE),str2xpos(ENDDATE))
    if y_label!=None:
        ylabel(y_label)
    else:
        ylabel("Total %s"%(name))
    xticks(rotation=40)
    legend(loc=2)
    quellenangabe(fig,"Data: Hopkins University")
    title(titletext)
    savefig("Corona_global_total_%s.png"%(name))
    fig=figure(name+"daily")
    ymin,ymax=0,ylim()[-1]
    ylim(ymin,ymax)
    plot_events(events)    
    xlim(str2xpos(STARTDATE),str2xpos(ENDDATE))
    quellenangabe(fig,"Data: Hopkins University")
    ylabel(u"Täglche %s mavg%s "%(name,avglen))
    title(u"Tägliche %s  mavg%s"%(name,avglen))
    xticks(rotation=40)
    legend(loc=2)
    savefig("Corona_global_daily_%s.png"%(name))
    
    fig=figure(name+"slope")
    ylim(0,5)
    plot_events(events)    
    xlim(str2xpos(STARTDATE),str2xpos(ENDDATE))
    quellenangabe(fig,"Data: Hopkins University")
    ylabel("%s-Tage Faktor %s "%(avglen,name))
    title("%s-Tage Steigerungsfaktor %s"%(avglen,name))
    xticks(rotation=40)
    legend(loc=2)
    savefig("Corona_global_slope_%s.png"%(name))

def plot_tote_und_positive(tote,positive,minval=0,selection=[],name="",ymin=0,ymax=None,titletext="",y_label=None,events=[]):
    print(selection)
    figure(name+"slope",figsize=(10,6))
    clf()
    grid() 
    figure(name+"daily",figsize=(10,6))
    clf()
    grid() 
    figure(name,figsize=(10,6))
    #fig,ax=subplots()
    clf()
    grid() 
    for country,vals in positive.countries.items():
        if max(vals)<minval:
            continue
        if len(selection)>0:
            if country not in selection:
                continue
        figure(name)
        plot(positive.dates,vals,ls="-",label="Positve %s"%country)
        figure(name+"daily")
        #plot(data.dates[1:],diff(vals),label=country)
        if len(selection)==1:
            color="g"
            ls="-"
            lw=3
        else:
            color=None
            ls=".-"
            lw=1
        plot(positive.dates[avglen:],(vals[avglen:]-vals[:-avglen])*1./avglen,ls,lw=lw,color=color,label="Positive %s"%country,alpha=0.8)
        figure(name+"slope")
        delta=(vals[avglen:]-vals[:-avglen])*1./avglen
        delta=where(delta>0,delta,1e-99)
        slope = delta[avglen:]/delta[:-avglen]
        slope=where(slope>1e90,0,slope)
        plot(positive.dates[2*avglen:],slope,label=country,lw=LINEWIDTH)

    figure(name)
    legend(loc=2)
    ylabel(u"Tägliche Positive mavg%s "%(avglen))
    twinx()
    figure(name+"daily")
    legend(loc=2)
    ylabel(u"Tägliche Positive mavg%s "%(avglen))
    twinx()

    for country,vals in tote.countries.items():
        if max(vals)<minval:
            continue
        if len(selection)>0:
            if country not in selection:
                continue
        figure(name)
        if len(selection)==1:
            color="k"
            ls="-"
            lw=3
        else:
            color=None
            ls="+-"
            lw=1
        plot(tote.dates,vals,ls,color=color,label="Tote %s"%country)
        figure(name+"daily")
        #plot(data.dates[1:],diff(vals),label=country)
        plot(tote.dates[avglen:],(vals[avglen:]-vals[:-avglen])*1./avglen,ls,lw=lw,color=color,label="Tote %s"%country,alpha=0.8)

        figure(name+"slope")
        delta=(vals[avglen:]-vals[:-avglen])*1./avglen
        delta=where(delta>0,delta,1e-99)
        slope = delta[avglen:]/delta[:-avglen]
        slope=where(slope>1e90,0,slope)
        if len(selection)==1:
            plot(tote.dates[2*avglen:],slope,"k",label=country,lw=LINEWIDTH,alpha=0.8)
        else:
            plot(tote.dates[2*avglen:],slope,"+-",label=country,lw=LINEWIDTH,alpha=0.8)


    fig=figure(name)
    if ymax==None:
        ymax=ylim()[-1]

    ylim(ymin,ymax)
    plot_events(events)    
    xlim(str2xpos(STARTDATE),str2xpos(ENDDATE))
    if y_label!=None:
        ylabel(y_label)
    else:
        ylabel("Total %s"%(name))
    xticks(rotation=40)
    legend(loc=1)
    quellenangabe(fig,"Data: Hopkins University")
    title(titletext)
    savefig("Corona_global_total_%s.png"%(name))
    fig=figure(name+"daily")
    ymin,ymax=0,ylim()[-1]
    ylim(ymin,ymax)
    plot_events(events)    
    xlim(str2xpos(STARTDATE),str2xpos(ENDDATE))
    quellenangabe(fig,"Data: Hopkins University")
    ylabel(u"Tägliche Tote mavg%s "%(avglen))
    title(u"Tägliche %s  mavg%s"%(name,avglen))
    xticks(rotation=40)
    legend(loc=1)
    savefig("Corona_global_daily_%s.png"%(name))
    
    fig=figure(name+"slope")
    ylim(0,5)
    plot_events(events)    
    xlim(str2xpos(STARTDATE),str2xpos(ENDDATE))
    quellenangabe(fig,"Data: Hopkins University")
    ylabel("%s-Tage Faktor %s "%(avglen,name))
    title("%s-Tage Steigerungsfaktor %s"%(avglen,name))
    xticks(rotation=40)
    legend(loc=2)
    savefig("Corona_global_slope_%s.png"%(name))



def density(data):
    outdata=copy.deepcopy(data)
    for country,vals in outdata.countries.items():
        if country in einwohner.countries:
            vals = array(vals)*1000.0/einwohner.countries[country]
            outdata.countries[country]=vals
        else:
            outdata.countries.pop(country)
    return outdata 

def relative(dataz,datan,mode=None):
    outdata=copy.deepcopy(dataz)
    outdata.dates=outdata.dates
    for country in outdata.countries.keys():
        n=datan.countries[country]
        z=dataz.countries[country]
        if mode=="survival":
            n = where(n>0,n,99e99)
            vals = (n-z)/n * 100
        else:
            n = where(n>0,n,99e99)
            vals = z/n
        outdata.countries[country]=vals
    return outdata

"""
wird nicht verwendet
def smooth(data,n=7):
    outdata=copy.deepcopy(data)
    outdata.dates=outdata.dates[7:]
    for country,vals in outdata.countries.items():
        vals = vals[7:]-vals[:-7]
        outdata.countries[country]=vals
    return outdata
"""

if __name__=="__main__":
    import sys

    events=[
        #("23.3.2020",u"Bundesweite Kontaktbeschränkungen"),
        #("29.4.2020",u"Bundsweite Maskenpflicht"),
        ("02.Nov.2020",u"Lockdown Light in Deutschland"),
        ("16.dec.2020",u"Verschärfter Lockdown in Deutschland"),
        ("26.dec.2020",u"Beginn der Impfungen in Deutschland"),
    ]


    selection = sys.argv[1:]
    tdata = loader("deaths")
    plot_countries(tdata,selection=selection,name="number of %s"%tdata.name)
    tddata=density(tdata)
    #plot_countries(ddata,selection=selection,name="number of %s per 1000 Inhabitans"%ddata.name)
    plot_countries(tddata,selection=selection,name="Anzahl Corona-Tote pro 1000 Einwohner",events=events)
    cdata = loader("confirmed")
    plot_countries(cdata,selection=selection,name="number of %s"%cdata.name)
    ddata=density(cdata)
    plot_tote_und_positive(tddata,ddata,selection=selection,name="Corona Tote und Positive pro 1000 Einwohner")
    plot_countries(ddata,selection=selection,name="Anzahl Testpositive pro 1000 Einwohner")
    rdata=relative(tdata,cdata)
    plot_countries(rdata,selection=selection,name="number of Deaths per confirmed Cases")

    tdata = loader("deaths")
    cdata = loader("confirmed")
    rdata=relative(tdata,cdata,mode="survival")
    plot_countries(rdata,selection=selection,name="Total Survival Rate per 100 confirmed Cases",ymin=80,ymax=100,titletext="COVID19 Survival Rate")



