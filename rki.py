#!/usr/bin/python
# vim: fileencoding=utf8
""" Corona Daten des Robert Koch Instituts
"""
import csv
from numpy import * 
import IPython
from IPython import embed
import pandas
import traceback
import cPickle
import datetime

def loader(filename="datasources/RKI_COVID19.csv",endtime=100):
        class Lkr:
            def __init__(self):
                self.fall={"total":{}}
                self.todesfall={"total":{}}
                self.genesene={"total":{}}
                self.bl=[]

        class data:
            endtime=0
            gesamt=Lkr()
            lkr={}
            bl={}
            fall={}
            todesfall={}
            genesene={}

        rdata=pandas.read_csv(filename,parse_dates=[8])
        #inf = file(filename)
        #cr = csv.reader(inf.readlines())
        #header = cr.next()
        #hkeys=header.split(',')
        for lineno in range(len(rdata.values)):
            datum = rdata["Meldedatum"][lineno]
            if type(datum)==pandas._libs.tslibs.timestamps.Timestamp:
                datum=datum.date()
            lkr = rdata["Landkreis"] [lineno].lower() #.decode("latin-1")#.encode("utf-8")
            bl = rdata["Bundesland"][lineno].lower()
            alter = rdata["Altersgruppe"][lineno]
            try:
                if alter.endswith("+"):
                    alter=999
                elif alter=="unbekannt":
                    alter=999
                else:
                    alter=int(alter[-2:])
                fall = int(rdata["AnzahlFall"][lineno])
                genesene=int(rdata["AnzahlGenesen"][lineno])   
                todesfall = int(rdata["AnzahlTodesfall"][lineno])
            except:
                traceback.print_exc()
                embed()
            if not data.lkr.has_key(lkr):
                    data.lkr[lkr]=lkrobj=Lkr()
                    data.lkr[lkr].bl=bl
            if not data.bl.has_key(bl):
                data.bl[bl]=blobj=Lkr()

            def add_data(dobj):
                if fall!=0:
                    if not datum in dobj.fall["total"]:
                        dobj.fall["total"][datum]=0
                    dobj.fall["total"][datum]+=fall		
                    if not alter in dobj.fall:
                        dobj.fall[alter]={}
                    if not datum in dobj.fall[alter]:
                        dobj.fall[alter][datum]=0
                    dobj.fall[alter][datum]+=fall
                if not datum in dobj.genesene["total"]:
                    dobj.genesene["total"][datum]=0
                dobj.genesene["total"][datum]+=genesene
                if todesfall!=0:
                    if not datum in dobj.todesfall["total"]:
                        dobj.todesfall["total"][datum]=0
                    dobj.todesfall["total"][datum]+=todesfall		
                    if not alter in dobj.todesfall:
                        dobj.todesfall[alter]={}
                    if not datum in dobj.todesfall[alter]:
                        dobj.todesfall[alter][datum]=0
                    dobj.todesfall[alter][datum]+=todesfall
            
            add_data(lkrobj)
            add_data(blobj)
            add_data(data.gesamt)
        return data

def timefill(d):
    try:
        date = d[0][0]
        vals = [d[0][1]]
        dates=[date]
        for date,val in d[1:]:
            while date - dates[-1]>datetime.timedelta(1):
                dates.append(dates[-1]+datetime.timedelta(1))
                vals.append(0)
            dates.append(date)
            vals.append(val)
    except:
        traceback.print_exc()
        embed()
    return array([dates,vals])

def dict2array(d):
    i = list(d.items())
    if len(i)==0:
        return []
    i.sort()
    d=timefill(i)
    return d

def export_pickles(data):
    try:
        lkrs = data.lkr.keys()
        lkrs.sort()
        allfall={}
        fall={}
        alltodesfall={}
        todesfall={}
        allgenesene={}
        genesene={}
        for lkr in lkrs:
            fall[lkr]={}
            allfall[lkr]={}
            for key in data.lkr[lkr].fall.keys():
                allfall[lkr][key]=fall[lkr][key]=dict2array(data.lkr[lkr].fall[key])

            todesfall[lkr]={}
            alltodesfall[lkr]={}
            for key in data.lkr[lkr].todesfall.keys():
                alltodesfall[lkr][key]=todesfall[lkr][key]=dict2array(data.lkr[lkr].todesfall[key])
            allgenesene[lkr]={}
            genesene[lkr]={}
            for key in data.lkr[lkr].genesene.keys():
                allgenesene[lkr][key]=genesene[lkr][key]=dict2array(data.lkr[lkr].genesene[key])
        f = open("datasources/faelle_lkr.rki","w")        
        cPickle.dump(fall,f)
        f.close()
        f = open("datasources/todesfaelle_lkr.rki","w")        
        cPickle.dump(todesfall,f)
        f.close()
        f = open("datasources/genesene_lkr.rki","w")        
        cPickle.dump(genesene,f)
        f.close()

        bls = data.bl.keys()
        bls.sort()
        fall={}
        todesfall={}
        genesene={}
        for bl in bls:
            fall[bl]={}
            allfall[bl]={}
            for key in data.bl[bl].fall.keys():
                allfall[bl][key]=fall[bl][key]=dict2array(data.bl[bl].fall[key])
            todesfall[bl]={}
            alltodesfall[bl]={}
            for key in data.bl[bl].todesfall.keys():
                alltodesfall[bl][key]=todesfall[bl][key]=dict2array(data.bl[bl].todesfall[key])
            genesene[bl]={}
            allgenesene[bl]={}
            for key in data.bl[bl].genesene.keys():
                allgenesene[bl][key]=genesene[bl][key]=dict2array(data.bl[bl].genesene[key])
        f = open("datasources/faelle_bl.rki","w")        
        cPickle.dump(fall,f)
        f.close()
        f = open("datasources/todesfaelle_bl.rki","w")        
        cPickle.dump(todesfall,f)
        f.close()
        f = open("datasources/genesene_bl.rki","w")        
        cPickle.dump(genesene,f)
        f.close()
       
        fall={}
        allfall["deutschland"]={}
        for key in data.gesamt.fall.keys():
            allfall["deutschland"][key]=fall[key]=dict2array(data.gesamt.fall[key])
        todesfall={}
        alltodesfall["deutschland"]={}
        for key in data.gesamt.todesfall.keys():
            alltodesfall["deutschland"][key]=todesfall[key]=dict2array(data.gesamt.todesfall[key])
        genesene={}
        allgenesene["deutschland"]={}
        for key in data.gesamt.genesene.keys():
            allgenesene["deutschland"][key]=genesene[key]=dict2array(data.gesamt.genesene[key])
        f = open("datasources/faelle_gesamt.rki","w")        
        cPickle.dump(fall,f)
        f.close()
        f = open("datasources/todesfaelle_gesamt.rki","w")        
        cPickle.dump(todesfall,f)
        f.close()
        f = open("datasources/genesene_gesamt.rki","w")        
        cPickle.dump(genesene,f)
        f.close()

        f = open("datasources/faelle.rki","w")        
        cPickle.dump(allfall,f)
        f.close()
        f = open("datasources/todesfaelle.rki","w")        
        cPickle.dump(alltodesfall,f)
        f.close()
        f = open("datasources/genesene.rki","w")        
        cPickle.dump(allgenesene,f)
        f.close()

    except:
        traceback.print_exc()
        embed()     
    
if __name__=="__main__":
	import sys
	data = loader()
        print("data loaded")
        export_pickles(data)
        print("export finieshed")
        embed()



