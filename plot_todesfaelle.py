#!/usr/bin/python
# vim: set fileencoding=utf8

import pandas
from IPython import embed
import traceback
from pylab import *
from matplotlib import patches
from isoweek import Week
from types import * 
from utils import *
import sterbefaelle

startdate=datetime.date(2020,1,1)
#startdate=datetime.date(2020,10,1)
#enddate=datetime.date(2020,12,31)

filename = "datasources/COVID-19_Todesfaelle.xlsx"
resultspath = "results/"

def quellenangabe(fig,rotation=90):
    msg = u"""Daten: RKI, COVID-19-Todesfälle.xlsx nach Sterbedatum, |Aufgrund von Testungenauigkeiten, wechselnden Teststrategien,
schwankenden Testzahlen, Mehrfachtestungen und falschen Totenscheinen sind die Zahlen mit Vorsicht zu betrachten."""
    if rotation==90:
        xpos,ypos=0.975,0.5
        valign="center"
        halign="center"
    else:
        xpos,ypos=0.5,0
        valign="bottom"
        halign="center"
    pylab.text(xpos,ypos,msg,rotation=rotation,fontsize=6, verticalalignment=valign, horizontalalignment=halign, transform=fig.transFigure)

def load_data():
    tote_bl = pandas.read_excel(filename,u"COVID_Todesfälle_BL",index_col=0,skiprows=0)
    tote = pandas.read_excel(filename,u"COVID_Todesfälle",index_col=0,skiprows=0,header=None)
    tote_monat = pandas.read_excel(filename,u"COVID_Todesfälle_Monat",index_col=0,skiprows=0,header=None)
    tote_ag = pandas.read_excel(filename,u"COVID_Todesfälle_KW_AG10",index_col=None,skiprows=0,header=None)
    tote_gag = pandas.read_excel(filename,u"COVID_Todesfälle_KW_AG20_G",index_col=0,skiprows=0,header=None)
    return (tote_bl,tote,tote_monat,tote_ag,tote_gag) 

def week2date(y,w=None):
    if not iterable(y):
        return Week(y,w).sunday()
    else:
        dates=[]
        for yi,wi in y:
            dates.append(week2date(yi,wi))
    return dates

def toint(data):
    if (not type(data) is type(u"")):
        try:
            outdata=[]
            for d in data:
                outdata.append(_toint(d))
            return outdata
        except:
            traceback.print_exc()
            embed() 
    else:
        return _toint(data)

def _toint(data):
    try:
        return int(data)
    except:
        traceback.print_exc()
        if data[0]=="<":
            val = int(data[1])/2
        else:
            val = int(data)
        return val

def load_tote_ag():
    data = pandas.read_excel(filename,u"COVID_Todesfälle_KW_AG10",index_col=None,skiprows=0,header=None)
    try:
        header = data.values[0][1:]
        data=data.sort_values(by=[0,1])[:-1]
        rows = data.iterrows()
        dates = week2date(data.values[1:,:2])
    except:
        traceback.print_exc()
        embed()
    ddict = {}
    ags =  header[:0:-1]
    for (i,ag) in enumerate(ags):
        d = toint(data.values[1:,-i-1])
        ddict[ag[3:]]=d
    return dates,ddict

def tote_gesamt(ddict=None):
    dates=None
    if ddict==None:
        dates,ddict=load_tote_ag()
    gesamt=zeros(len(ddict.values()[0]))
    for av in ddict.values():
        try:
            gesamt+=av
        except:
            traceback.print_exc()
            embed()
    if dates==None:   
        return gesamt
    return dates,gesamt
            
def load_tote_ag_summe():
    dates,ddict = load_tote_ag()
    ags = ddict.keys()
    ags.sort()
    ags = ags[4:]
    sums = []
    for i,ag in enumerate(ags):
        sums.append(sum(ddict[ag]))        
    return ags,sums

def plot_tote_gesamt(events=[],ymin=None,ymax=None,plot_summe=False,relativ_sf=False):
    dates,data = tote_gesamt()
    if relativ_sf:
        indates=dates[:]
        indata=data[:]
        dates=[]
        data=[]
        sfdates,sfges=sterbefaelle.load_gesamt_woche()
        sumval=0
        sumsfval=0
        for date,val in zip(indates,indata):
            if date in sfdates:
                # Warning: Problem bei Datenluecken
                ix = sfdates.index(date)
                sumsfval+=sfges[ix]
                sumval+=val
                data.append(sumval*100./sumsfval)
                dates.append(date)
    fig = figure(figsize=(10,6))
    clf()
    grid()
    xticks(rotation=30)
    try:
        c="b"
        if plot_summe:
            if relativ_sf:
                plot(dates,data,"-o",color=c)
            else:
                plot(cumsum(dates),data,"-o",color=c)
        else:
            plot(dates,data,"-o",color=c)
    except:
        traceback.print_exc()
        embed()
    if ymax==None:
        ymax=ylim()[1]
    if ymin==None:
        ymin=ylim()[0]
    for event in events:
        try:
            edate = str2xpos(event[0])
            plot([edate,edate],[ymin,ymax],color="k",lw=3,ls="--",alpha=0.4)
            text(edate+datetime.timedelta(1),(ymin+ymax)/2,event[1],rotation=90.0,alpha=0.4
                 ,horizontalalignment="left"
                ,verticalalignment="bottom"
                ,fontsize=10
                #,transform=fig.transFigure
                )
        except:
            traceback.print_exc()
            embed()
    ylim(ymin,ymax)
    legend(loc=2)
    quellenangabe(fig)
    if plot_summe:
        if relativ_sf:
            ylabel(u"Aufsummierte Sterbefälle")
            title(u"Anteil an oder mit Corona Verstorbener an Gesamtzahl Verstorbener (aufsummiert)")
            savefig("%s/Todesfaelle_relativ_Gesamtverstorbene_aufsummiert.png"%(resultspath))
        else:
            ylabel(u"Aufsummierte Sterbefälle")
            title(u"An oder mit Corona Verstorbene (aufsummiert)")
            savefig("%s/Todesfaelle_gesamt_absolut_aufsummiert.png"%(resultspath))
    else:
        if relativ_sf:
            ylabel(u"Anteil Sterbefälle / %")
            title(u"Anteil wöchentlich an oder mit Corona Verstorbene an Gesamtzahl Verstorbener")
            savefig("%s/Todesfaelle_gesamt_relativ_gesamt_Verstorbene.jpg"%(resultspath))
        else:
            ylabel(u"Sterbefälle / Woche")
            title(u"Wöchentlich an oder mit Corona Verstorbene")
            savefig("%s/Todesfaelle_gesamt_absolut.png"%(resultspath))

def calc_ag_ratio(zdates,zdict,ndates,ndict):
    lastag=0
    outdict={}
    for ag in zdict.keys():
        outdict[ag]=[]
        for zd,z in zip(zdates,zdict[ag]):
            if zd in ndates:
                ix = ndates.index(zd)
                n = ndict[ag][ix]
                outdict[ag].append(z*100.0/n)
    return outdict

def plot_tote_ag(events=[],ymin=None,ymax=None,plot_summe=False,relative_sf=False,altersverteilung=False):
    dates,ddict = load_tote_ag()
    if altersverteilung:
        gesamt = tote_gesamt(ddict)
    fig = figure(figsize=(10,6))
    clf()
    grid()
    xticks(rotation=30)
    ags = ddict.keys()
    ags.sort()
    if relative_sf:
        sfdates,sfag=sterbefaelle.load_altersgruppen_woche()
        outags=range(30,91,10)
        outags.append(999)
        dates,ddict=calc_ag_ratio(dates,agregroup(outtags,ddict),sfdates,agregroup(outag,sfag))
        ags=outags
    try:
        for i,ag in enumerate(ags):
        #for i,ag in enumerate(header[2:]):
            c = cm.jet(((i+1)*1.0/(len(ags))))
            #embed()
            print((ag,i))
            #d = toint(data.values[1:,i+2])
            if plot_summe:
                plot(dates,cumsum(ddict[ag]),"-o",color=c,label=ag)
            else:
                if relative_sf:
                    plot(dates,ddict[ag],"-o",color=c,label=ag)
                else:
                    if altersverteilung:
                        plot(dates,array(ddict[ag])*100./gesamt,"-o",color=c,label=ag)
                    else:
                        plot(dates,ddict[ag],"-o",color=c,label=ag)
    except:
        traceback.print_exc()
        embed()
    if ymax==None:
        ymax=ylim()[1]
    if ymin==None:
        ymin=ylim()[0]
    for event in events:
        try:
            edate = str2xpos(event[0])
            plot([edate,edate],[ymin,ymax],color="k",lw=3,ls="--",alpha=0.4)
            text(edate+datetime.timedelta(1),(ymin+ymax)/2,event[1],rotation=90.0,alpha=0.4
                 ,horizontalalignment="left"
                ,verticalalignment="bottom"
                ,fontsize=10
                #,transform=fig.transFigure
                )
        except:
            traceback.print_exc()
            embed()
    ylim(ymin,ymax)
    legend(loc=2)
    quellenangabe(fig)
    if plot_summe:
        ylabel(u"Aufsummierte Sterbefälle")
        title(u"An oder mit Corona Verstorbene (aufsummiert)")
        savefig("%s/Todesfaelle_vs_Alter_absolut_aufsummiert.png"%(resultspath))
    else:
        if altersverteilung:
            ylabel(u"Anteil Sterbefälle / Woche")
            title(u"Altersverteilung der wöchentlich an oder mit Corona Verstorbenen")
            savefig("%s/Todesfaelle_vs_Alter_Verteilung.png"%(resultspath))
        else:
            ylabel(u"Sterbefälle / Woche")
            title(u"Wöchentlich an oder mit Corona Verstorbene")
            savefig("%s/Todesfaelle_vs_Alter_absolut.png"%(resultspath))

def plot_tote_ag_pie():
    labels,sums = load_tote_ag_summe()
    dates,ddict = load_tote_ag()
    colors=[]
    for i in range(len(labels)):
        colors.append(cm.jet(((i+1)*1.0/(len(labels)))))
    fig = figure(figsize=(7,7))
    clf()
    grid()
    pie(sums,labels=labels,autopct="%3.1f%%",colors=colors,startangle=0)
    quellenangabe(fig,rotation=0)
    title("Altersverteilung an oder mit Corona Verstorbener")
    savefig("%s/Todesfaelle_vs_Alter_sumpie.png"%(resultspath))


if __name__=="__main__":

    events=[
        #("23.3.2020",u"Bundesweite Kontaktbeschränkungen"),
        ("29.4.2020",u"Bundsweite Maskenpflicht"),
        ("02.Nov.2020",u"Lockdown Light"),
        #("03.Nov.2020",u"Änderung der Testkriterien"),
        #("16.Nov.2020",u"Erwarteter Wirkungseintritt Lockdown Light"),
        ("16.dec.2020",u"Verschärfter Lockdown"),
        #("30.dec.2020",u"Erwarteter Wirkungseintritt verschärfter Lockdown"),
        #("16.jan.2021",u"Wandergruppe Mühlheim"),
    ]
    
    plot_tote_ag(events=events,ymin=0,altersverteilung=True)
    plot_tote_gesamt(events=events,ymin=0,relativ_sf=True,plot_summe=True)
    plot_tote_gesamt(events=events,ymin=0,relativ_sf=True)
    plot_tote_gesamt(events=events,ymin=0)
    plot_tote_ag_pie()
    plot_tote_ag(events=events,ymin=0)
    plot_tote_ag(events=events,ymin=0,plot_summe=True)

