#!/usr/bin/python
# vim:fileencoding=utf8


import rki
import IPython
import scipy.optimize as so
import datetime
import traceback
import pandas
import einwohner
from pylab import *


altercolors={"A00-A04":"b","A05-A34":"c","A15-A34":"tab:pink","A35-A59":"g","A60-A79":"tab:orange","A80+":"r","total":"k"}


def plot_todesanteil(data,land="Deutschland",alter="all"):    
    land=land.lower()
    alter=alter.lower()

    if land=="deutschland":
        d = data.gesamt
    else:
        for bl in data.bl.keys():
            if land in bl:
                land=bl
                break
        if land in data.bl:
            d = data.bl[land]
        else:
            for lkr in data.lkr.keys():
                if land in lkr:
                    land=lkr
                    break
            d = data.lkr[land]
    land=land.decode("ascii","ignore")
    alters=d.todesfall.keys()
    alters.sort()
    figure(land+" sum",figsize=(10,6))
    grid()
    figure(land,figsize=(10,6))
    grid()
    for alter in alters:
        color = altercolors[alter]
        tdict = d.todesfall[alter]
        fitems = d.fall[alter].items()  
        fitems.sort()
        fitems = array(fitems)
        x = []
        y = []
        totesum=0
        faellesum=0
        ysum = []
        for ti,f in fitems:
            faellesum += f
            if ti in tdict:
                to = tdict[ti]
                y.append(to*1000.0/f)
                totesum += to
            else:
                y.append(0)
            if faellesum>0:
                ysum.append(totesum *1000.0/faellesum)
            else:
                ysum.append(0)
            x.append(ti)
            # wenn es an einem Tag Tote, aber keine Faelle  gibt, dann gehen die Toten unter. Vernachlässigbar.
        figure(land)
        plot(x,y,label=alter,color=color)
        figure(land+" sum")
        plot(x,ysum,label=alter,color=color)
        print((alter,faellesum,totesum))
    xticks(rotation=5)
    legend(loc=2)
    ylabel("Summe der Sterbefaelle pro 1000 positiv Getestete")
    title("Summe der Coronasterbefaelle pro 1000 positiv Getestete in %s"%land.capitalize())
    savefig("Coronatote_zu_faelle_summe_%s.png"%land)
    figure(land)
    xticks(rotation=5)
    legend(loc=2)
    ylabel("Sterbefaelle pro 1000 positiv Getestete pro Tag")
    title("Taegliche Coronasterbefaelle pro 1000 positiv Getestete in %s"%land.capitalize())
    savefig("Coronatote_zu_faelle_pro_tag_%s.png"%land)

if __name__=="__main__":    
    import sys
    data = rki.loader()
    if len(sys.argv)==1:
        plot_todesanteil(data)
    else:
       for land in sys.argv[1:]:
           plot_todesanteil(data,land=land)
