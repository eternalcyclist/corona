#!/usr/bin/python
# vim: fileencoding=utf8

from pylab import *
import numpy as np
import scipy.optimize as so
import scipy.interpolate as si
import einwohner
import rki
import sys
import traceback
import IPython
from IPython import embed


def extract_alter(alters,vals,rescale=False):
    print("extract_alter") 
    abins=[] #zeros(90/5)  # wir splitten die Altersgruppenbis 90 in 5Jahrs-Bins
    alter=[] 
    lasta = alters[0]
    lastn = vals[0]
    #alters=append(alters,90)
    #vals=append(vals,0)
    for ar,n in zip(alters[1:],vals[1:]):
        nsum = sum(lastn)
        astart=lasta
        astop=ar
        lastn = n
        lasta = ar
        am = (1.0*astop+astart)/2
        if not rescale:
            l = (astop-astart+1)*1.0
            nsum = nsum*1.0/l
        alter.append(am)
        abins.append(nsum)
    alter.append(90.0)    # hier nehme wir nicht die Mitte des Intervals, sondern das Ende. Is a bisserl doof!
    abins.append(sum(lastn)*1.0/(90.-lasta+1))
    """
    if not rescale:
        mask = where(abins!=0)
        abins = take(abins,mask)[0]
        alter = take(alter,mask)[0]
    """
    alter=array(alter)
    abins=array(abins)
    print(("alter,abins",alter,abins))
    return alter,abins

def equal_split_alter(alters,vals):
    print(("equal_split_alter",alters,vals)) 
    abins=zeros(90/5)  # wir splitten die Altersgruppenbis 90 in 5Jahrs-Bins

    lasta = alters[0]
    lastn = vals[0]
    #alters=append(alters,90)
    for alter,n in zip(alters[1:],vals[1:]):
        nsum = sum(lastn)
        print(("alter,nsum",alter,nsum))
        astart=lasta
        astop=alter
        lasta=alter
        lastn=n
        for aix in range(astart/5,astop/5):
            print(("aix",aix))
            alen=(astop-astart)/5+1
            abins[aix]=nsum*1.0/alen
    return abins

def estimate_alter_curve(alter,abins,estfun):
    print("estimate_alter_curve %s"%estfun)
    print(("alter",alter))
    print(("abins",abins))

    def verlauf(x,a,b):
            return a*x**b

    #coeffs = polyfit(alter,abins,2)
    if estfun!="t":
        endalter=85
    else:
        endalter=alter[-1]
    print("endalter %s"%endalter)
    alterfine=arange(alter[0],endalter+5,5)
    if estfun=="t":
        popt,dummy = so.curve_fit(verlauf,alter,abins,absolute_sigma=False)
        #vals = polyval(coeffs,alterfine)
        vals = verlauf(alterfine,*popt)
    else:
        print(alter,abins,alterfine)
        vals = si.griddata(alter,abins,alterfine,method="cubic")
    return alterfine,vals

def plot_altersverlauf(obj,name):
    print("plot_altersverlauf %s"%name)
    clf()
    grid()
    if "Tote" in name:   
        estfun="t"
        abins = equal_split_alter(obj.alter,obj.todesfall_alter)
        print (("abins nach equalsplit",abins))
        alterx = arange(0,90,5)
        alterf,valsf=estimate_alter_curve(alterx,abins,estfun)
        bar(alterf,valsf,color="r",width=5)
        bar(alterx,abins,color="k",alpha=0.4)
    else:
        altero,vals = extract_alter(obj.alter,obj.fall_alter,rescale=False)
        print(altero)
        estfun="p"
        print(len(altero),len(vals))
        alterf,valsf=estimate_alter_curve(altero,vals,estfun)
        bar(alterf-2.5,valsf,color="b",width=5)
        bar(altero,vals,color="k",alpha=0.4)
    ylabel(name.split()[0])
    xlabel("Alter")
    title("Geschaetzter Altersverlauf Corona %s "%name)
    print(valsf)
    print(("valsf",sum(valsf)))
    savefig("Altersverlauf_%s.png"%name)

if __name__=="__main__":

    import sys
    selected=sys.argv[1:]

    data = rki.loader()
    einwohner_lkr=einwohner.loader()

    fall=0
    todesfall=0
    todesfall_alter=None
    fall_alter=None
    for lkr,lo in data.lkr.items():
        print(lkr)
        for sel in selected:
            if sel.lower() in lkr.lower():
                plot_altersverlauf(lo,"Tote in %s"%lkr)
                plot_altersverlauf(lo,"Positiv Gemeldete in %s"%lkr)

        if lkr=="SK Berlin": continue    # da haben wir die Bezirke separat
        todesfall+=sum(lo.todesfall["total"].values())
        fall+=sum(lo.fall["total"].values())
        """
        if todesfall_alter==None:
            todesfall_alter=lo.todesfall_alter
        else:
            todesfall_alter +=lo.todesfall_alter
        if fall_alter==None:
            fall_alter=lo.fall_alter
        else:
            fall_alter +=lo.fall_alter
        """
        #for alter,f,t in zip(lo.alter,todesfall_alter,fall_alter):
        #    print("%s %s %s"%(alter,f,t))
    print(todesfall)
    print(sum(todesfall))
    print(sum(fall))

    class dobj:
        alter = lo.alter
        todesfall_alter=todesfall_alter
        fall_alter=fall_alter

    print(dobj.todesfall_alter)
    print(dobj.fall_alter)
    plot_altersverlauf(dobj,"Tote in Deutschland")
    plot_altersverlauf(dobj,"Positive Getesten in Deutschland")




