#!/usr/bin/python
# vim: fileencoding=utf-8

from pylab import *
import scipy.optimize as so
from IPython import embed

startyear=1950
mlist=[
        "Jan","Feb","Mar","Apr","Mai","Jun","Jul","Aug","Sep","Okt","Nov","Dez"
        ]

def load():
    months=[] #arange(((2020-startyear)*12))
    vals=[] #zeros(((2020-startyear+1)*12))
    
    """
    for line in open("datasources/12613-03-01-4-B.csv").readlines():
        line = line.strip()
        if len(line)==0:
            continue
        words = line.split(";")
        if len(words)==0:
            continue
        if len(words[0])==0:
            continue
        if not words[0][0]=="2":
            continue
        year = int(words[0])
        for m in range(12):
            print((year,m))
            try:
                val= int(words[4+m])
            except:
                break
            mi = (year-startyear)*12+m
            vals[mi]=val
    """
    for line in open("datasources/12613-0006.csv").readlines():
        line = line.strip()
        words = line.split(";")
        print(words)
        if len(words)<3:
            continue
        if len(words[0])==0:
            continue
        if words[0][0] not in "12":
            continue
        print(line)
        if words[2]=="...":
            break
        year = int(words[0])
        try:
            month = mlist.index(words[1][:3])
        except ValueError:
            month=2
        mi = (year-startyear)*12+month
        print(words)
        print(mi)
        try:
            fval =int(words[2])
            mval =int(words[3])
        except:
            print(words)
            continue
        try:
            vals.append(mval+fval)
            months.append(mi)
        except:
            print("Exception")
            embed()
    return array(months),array(vals)

def smooth(vals):

    def curve(x,off,steigung,per,ampl,phi):
        #return off+steigung*x + ampl*sin(x*per+phi)
        return off+steigung*x + ampl*sin(6.28*x/12+phi)

    try:
        vals = 1.0*vals
    except:
        print("Exception in smooth")
        embed()
    x = arange(len(vals),dtype=float) 
    popt,dummy = so.curve_fit(curve,x,vals,(80000,10,10000,2*3.14/12,3.14/2))
    print(popt)
    svals = curve(x,*popt)
    return svals


def plot_gestorbene(months,vals):
    msum = cumsum(vals)
    clf()
    grid()
    m = months[-121:-1]
    v = vals[-121:-1]
    plot(m,v,"-o")
    svals = smooth(v)
    plot(m,svals)
    mavg=msum[-121+12:-1]-msum[-121:-13]
    print(len(mavg))
    print(len(m[:-12-1]))
    plot(m[:-12],mavg/12)
    xticks(range(0,70*21,12),range(startyear,2020),rotation=60)
    #xlim(min(months),max(months))
    xlim(months[-121],max(months))
    title("Monatlich Gestorbene in Deutschland")
    xlabel("Jahr")
    ylabel("Gestorbene pro Monat")
    show()
    clf()
    grid()
    plot(m,v-svals)
    xlabel("Jahr")
    ylabel("Abweichung Gestorbene pro Monat")
    title("Abweichung vom erwarteten Werte monatlich Gestorbene in Deutschland")
    xticks(range(0,70*21,12),range(startyear,2020),rotation=60)
    xlim(months[-121],max(months))
    show()

    yvals = []
    for m in arange(months[0],months[-1],12):
        yvals.append(sum(vals[m:m+12]))
    clf()
    grid()
    bar(arange(startyear,2020)-0.5,yvals)
    ylim(min(yvals),max(yvals))
    #xticks(range(0,70,12),range(startyear,2020),rotation=60)
    show()

    

if __name__=="__main__":
    m,v=load()
    plot_gestorbene(m,v)


