#!/usr/bin/python
# vim: set encoding=utf8

import pandas
from IPython import embed
import traceback
from pylab import *
import pylab
from matplotlib import patches
from isoweek import Week

filename = "datasources/Testzahlen-gesamt.xlsx"

def quellenangabe(fig):
    msg = u"""Daten: RKI Testzahlen-gesamt.xlsx |Aufgrund von Testungenauigkeiten, wechselnden Teststrategien,
schwankenden und nicht vollständig gemeldeten Testzahlen, Mehrfachtestungen sind die Zahlen mit Vorsicht zu betrachten."""
    pylab.text(0.975,0.5,msg,rotation=90,fontsize=6, verticalalignment="center", transform=fig.transFigure)

def week2date(wstr):
    print("wstr %s"%wstr)
    w,y= map(int,wstr.strip("*").split("/"))
    d = Week(y,w).sunday()
    return d

def tkweek2date(winstr):
    """ Testkapaziäten hat ein anderes Format als Testzahlerfasssung
    """
    try:
        ystr,wstr=winstr.split(",")
        wstr=wstr.strip()
        y=int(ystr)
        print(wstr)
        w=int(wstr[2:])
        d = Week(y,w).sunday()
    except:
        traceback.print_exc()
        embed()
    return d

def load_testzahlen():
    tz = pandas.read_excel(filename,"1_Testzahlerfassung",index_col=None,skiprows=[1],header=0,skipfooter=4,converters={0:week2date})
    return tz

def load_testkapazitaeten():
    try:
        tk = pandas.read_excel(filename,u"2_Testkapazitäten",skiprows=1,index_col=None,converters={0:tkweek2date})
    except:
        traceback.print_exc()
        embed()
    return tk

def load_testkap():
    tk = load_testkapazitaeten()
    dates=tk.values[:,0]
    rk = tk.values[:,4]
    tk = tk.values[:,3]
    return dates,tk,rk

def load_anzahltests():
    tz = load_testzahlen()
    dates=tz.values[1:,0]
    az = tz["Anzahl Testungen"][1:] 
    return dates,az

def load_laborauslastung():
    kdates,tk,rk=load_testkap()
    ndates,nt = load_anzahltests()
    ldates=[]
    la=[]
    try:
        for i,nd in enumerate(ndates):
            try:
                kix = where(kdates==nd)[0][0]
                rki = rk[kix]
            except:
                traceback.print_exc()
                continue
            ldates.append(nd)
            la.append(nt.values[i]*100.0/rki)
    except:
        traceback.print_exc()
        embed()
    return ldates,la

def load_anzahlpositive():
    tz = load_testzahlen()
    dates=tz.values[1:,0]
    pz = tz["Positiv getestet"][1:] 
    return dates,pz

def load_positivenquote():
    tz = load_testzahlen()
    pq=tz["Positivenquote (%)"][1:]
    dates =tz.values[1:,0]
    return dates,pq

def load_anzahllabore():
    tz = load_testzahlen()
    lz = tz[u"Anzahl übermittelnder Labore"][1:]
    dates = tz.values[1:,0]
    return dates,lz

def plot_testzahlen(startdate=None,enddate=None):
    fig=figure(figsize=(12,6))
    clf()
    grid(1)
    title("Anzahl Testungen und Anzahl Positive pro Woche")
    try:
        #plot(tz.values[1:-2,0],tz["Anzahl Testungen"][1:-2],"g",lw=2,label="Anzahl Testungen")
        dates,tz=load_anzahltests()
        plot(dates,tz,"g-o",lw=2,label="Anzahl Testungen")
        dates,pz=load_anzahlpositive()
        plot(dates,pz,"b-o",lw=2,label="Anzahl Positive")
        ymin,ymax=ylim()
    except:
        traceback.print_exc()
        embed()
    #patches.rectangle((36,0),
    #plot([36,36],[0,ylim()[-1]],"r",lw=20,alpha=0.25)
    #bar(36,ylim()[-1],"r",width=len(tz.columns)-3-36, alpha=0.25,align="edge")
    ylabel("Anzahl Testungen\nAnzahl Positive")
    xticks(rotation=30)
    legend(loc=2)
    try: 
        xlim(startdate,enddate)
    except:
        try:
            enddate=max(dates)
            xlim(startdate,enddate)
            print("using only startdate")
        except:
            traceback.print_exc()
            print("auto start and end date")
            startdate=min(dates)
            enddate=max(dates)
            xlim(startdate,enddate)
    ymax=max(tz)*1.1
    bar(datetime.date(2020,11,2),ymax,width=1e6,color="r", alpha=0.15,align="edge")
    text(datetime.date(2020,11,5),ymax,"Andere Testkriterien",color="r",alpha=0.5,rotation=0,fontsize=12,verticalalignment="top")
    twinx()
    dates,pq=load_positivenquote()
    plot(dates,pq,"r-o",lw=3,label="Postivenquote")
    ylim(0,ylim()[1])
    ylabel("Postivenquote / %")
    legend(loc=4)
    quellenangabe(fig)
    savefig("Corona-Testzahlen.png")
    #show()

def plot_anzahllabore(startdate=None,enddate=None):
    dates,lz=load_anzahllabore()
    fig=figure(figsize=(12,6))
    clf()
    grid(1)
    xticks(rotation=30)
    title(u"Anzahl übermittlender Labore und Positivenquote pro Woche")
    plot(dates,lz,"g-o",lw=3,label=u"Anzahl übermittelnder Labore")
    ymin,ymax=ylim()
    try: 
        xlim(startdate,enddate)
    except:
        try:
            enddate=max(dates)
            xlim(startdate,enddate)
            print("using only startdate")
        except:
            traceback.print_exc()
            print("auto start and end date")
            startdate=min(dates)
            enddate=max(dates)
            xlim(startdate,enddate)
    ymax = max(lz)*1.1
    bar(datetime.date(2020,11,2),ymax,width=1e6,color="r", alpha=0.15,align="edge")
    text(datetime.date(2020,11,5),ymax,"Andere Testkriterien",color="r",alpha=0.5,rotation=0,fontsize=12,verticalalignment="top")
    ylabel("Anzahl Labore")
    legend(loc=2)
    #ylim(150,ymax) 
    ylim(150,ymax) 
    text(37,200,"Andere Testkriterien",color="r",alpha=0.5,rotation=90,fontsize=16)
    twinx()
    dates,pq=load_positivenquote()
    plot(dates,pq,"r-o",lw=3,label="Postivenquote")
    ylim(0,ylim()[1])
    ylabel("Postivenquote / %")
    legend(loc=4)
    quellenangabe(fig)
    savefig("Corona-Testlaboranzahl_und_Quote.png")
    #show()
    fig=figure(figsize=(12,6))
    clf()
    grid(1)
    xticks(rotation=30)
    title(u"Anzahl übermittlender Labore und Anzahl Positive pro Woche")
    plot(dates,lz,"g-o",lw=3,label=u"Anzahl übermittelnder Labore")
    ymin,ymax=ylim()
    try: 
        xlim(startdate,enddate)
    except:
        try:
            enddate=max(dates)
            xlim(startdate,enddate)
            print("using only startdate")
        except:
            traceback.print_exc()
            print("auto start and end date")
            startdate=min(dates)
            enddate=max(dates)
            xlim(startdate,enddate)
    ymax = max(lz)*1.1
    bar(datetime.date(2020,11,2),ymax,width=1e6,color="r", alpha=0.15,align="edge")
    text(datetime.date(2020,11,5),ymax,"Andere Testkriterien",color="r",alpha=0.5,rotation=0,fontsize=12,verticalalignment="top")
    ylabel("Anzahl Labore")
    legend(loc=2)
    ylim(150,ymax) 
    text(37,200,"Andere Testkriterien",color="r",alpha=0.5,rotation=90,fontsize=16)
    twinx()
    dates,pz=load_anzahlpositive()
    plot(dates,pz,"b-o",lw=3,label="Anzahl Positive")
    ylim(0,ylim()[1])
    ylabel("Anzahl Positive")
    legend(loc=4)
    quellenangabe(fig)
    savefig("Corona-Testlaboranzahl_und_Positive.png")

def plot_laborauslastung():
    dates,tk,rk=load_testkap()
    ndates,nz=load_anzahltests()
    fig=figure(figsize=(12,6))
    clf()
    grid(1)
    plot(dates,rk,"g-o",label=u"reale Laborkapazität")
    plot(dates,tk,"b-o",label=u"theor. Laborkapazität")
    plot(ndates,nz,"r-o",label=u"Anzahl Tests")
    legend(loc=2)
    quellenangabe(fig)
    """
    twinx() 
    pos = tz["Positiv getestet"][1:-2]
    plot(pos,"r",lw=2,label="Anzahl Positive")
    legend(loc=4)
    """
    savefig("Corona-Testkapaziäten.jpg")

    fig=figure(figsize=(12,6))
    clf()
    grid(1)
    ldates,laus = load_laborauslastung()
    plot(ldates[2:],laus[2:],"r-o",lw=3,label="Laborauslastung")
    ylim(0,100)
    legend(loc=2)
    ylabel("Laborauslastung in Prozent")
    if 0:
        twinx()
        pdates,pz=load_anzahlpositive()
        plot(pdates,pz,"m-o",label="positive Tests",lw=3,alpha=0.5)
        legend(loc=4)
        ylabel("Positive Tests")
    title("Auslastung der SARS-CoV2 Testlabore in Deutschland")
    quellenangabe(fig)
    savefig("Corona_Laborauslastung.jpg")
    


if __name__=="__main__":
    plot_laborauslastung() 
    plot_testzahlen() 
    plot_anzahllabore() 


