#!/usr/bin/python
# vim: fileencoding=utf8

import csv
import scipy.interpolate as si
import numpy as np
import pylab as pl
import pandas as pd
import IPython
from IPython import embed
import traceback


countries={
        "Austria":8773e3,
        "Brazil":209500e3,
        "Germany":82522e3,
        "Finnland":5503e3,
        "France":66989e3,
        "Italy":60589e3,
        "Israel":9053e3,
        "Japan":126.5e6,
        #"Norway":5258e3,
        "Spain":46528e3,
        "Sweden":10e6,
        "Switzerland":8.4e6,
        "United Kingdom":65809e3,
        "US":328.2e6,
        }

abk_laender={
    "SH":"Schleswig-Holstein",
    "HH":"Hamburg",
    "HB":"Bremen",
    "NI":"Niedersachsen",
    "NW":"Nordrhein-Westfalen",
    "BW":"Baden-Württemberg",
    "BY":"Bayern",
    "HE":"Hessen",
    "MV":"Mecklenburg-Vorpommern",
    "RP":"Rheinland-Pfalz",
    "SL":"Saarland",
    "BE":"Berlin",
    "BB":"Brandenburg",
    "SN":"Sachsen",
    "ST":"Sachsen-Anhalt",
    "TH":"Thüringen",
    "D":"Deutschland"
}

def loader(filename="datasources/landkreise.csv",binwidth=None,tolower=False,ew_only=False):
        
	data={}	

	inf = file(filename)
	dia = csv.excel
	dia.delimiter=";"
	cr = csv.reader(inf.readlines(),dia)
	cr.next()
	cr.next()
	cr.next()
	cr.next()
	cr.next()
	header = cr.next()
	print(header)
	while(1):
		try:
			dl = cr.next()
		except StopIteration:
			break
		lkrstr  = dl[2]
		if "," in lkrstr:
			lkr,typ = lkrstr.split(",")
		else:
			lkr = lkrstr
			typ="Landkreis"
		if "(" in lkr:
			lkr = lkr.split("(")[0]
		if "Lindau" in lkr:
			lkr = "Lindau"
			typ = "Landkreis"
		if lkr=="Bodenseekreis":
			typ="Landkreis"
		if "Freiburg" in lkr:
			lkr = "Freiburg"
		if typ.strip()=="Landkreis":
			lkr = "LK %s"%lkr			
		else:
			lkr = "SK %s"%lkr
		lkr = lkr.decode("latin-1").encode("utf-8")
		if not dl[-1].isdigit():
			continue	
                if tolower:
                    lkr = lkr.lower()
                if ew_only:
                    ew = int(dl[-1])
                    data[lkr]=ew
                else:
                    data[lkr]={"alter":{}}
                    ew = int(dl[-1])
                    data[lkr]["ew"]=ew
                    abins=np.array([0,3,6,10,15,18,20,25,30,35,40,45,50,55,60,65,75])
                    binvals = []
                    for i,a in enumerate(abins):
                        binvals.append(int(dl[3+i]))       
                    binvals = np.array(binvals)
                    if binwidth!=None:
                        #abins = np.append(abins,90)
                        nabins = np.arange(0,90,binwidth)                              
                        print((abins,binvals,nabins))
                        binvals = si.griddata(abins,binvals,nabins,fill_value=0)
                        abins = nabins
                    data[lkr]["alter"]=(abins,binvals)

	return data

def load_laender(tolower=False,latest=False,intausend=True):
    data={}
    ew=pd.read_csv("datasources/laender.txt",sep="\t",skiprows=[0,1])
    for land,vals in zip(ew.columns[1:],ew.values.swapaxes(0,1)[1:]):
        if not intausend:
            vals=map(lambda x:1000*x,vals)
        landl = abk_laender[land].lower()
        if tolower:
            landl = landl.lower()
        if latest:
            data[landl]=vals[-1]
        else:
            data[landl]={}
            for year,val in zip(ew.values[:,0],vals):
                data[landl][year]=val
    return data

def load_all(tolower=True):
    try:
        data=load_laender(tolower=tolower,latest=True,intausend=False)
        data.update(loader(tolower=tolower,ew_only=True))
    except:
        traceback.print_exc()
        embed()
    return data


if __name__=="__main__":
        dl = load_laender()
        IPython.embed()
	data = loader(binwidth=5)
        for lkr in ["LK Lindau","LK Tuttlingen","LK Bodenseekreis","LK Biberach"]:
            print(lkr)
            print(data[lkr])
            asum=0
            asum=np.sum(data[lkr]["alter"][1])
            print(asum)
            print(data[lkr]["ew"])


